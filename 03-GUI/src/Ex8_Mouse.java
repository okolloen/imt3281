import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex8_Mouse extends JFrame { 
	private JPanel mousePanel;
	private JLabel statusBar;

	public Ex8_Mouse () {
		super ("Mouse tracking example");
		mousePanel = new JPanel ();
		add (mousePanel, BorderLayout.CENTER);
		statusBar = new JLabel ("Mouse is outside the panel"); 
		add (statusBar, BorderLayout.SOUTH);
		MouseHandler handler = new MouseHandler (); 
		mousePanel.addMouseListener (handler); 
		mousePanel.addMouseMotionListener (handler);
	}
	
	private class MouseHandler implements MouseListener, MouseMotionListener { // MouseListener events
		public void mouseClicked (MouseEvent me) {
			statusBar.setText (String.format ("Klikket på [%d, %d]", me.getX(), me.getY())); 
		}
		
		public void mousePressed (MouseEvent me) {
			statusBar.setText (String.format ("Trykket på [%d, %d]", me.getX(), me.getY()));
		}
		
		public void mouseReleased (MouseEvent me) {
			statusBar.setText (String.format ("Sluppet opp på [%d, %d]", me.getX(), me.getY()));
		}
		
		public void mouseEntered (MouseEvent me) {
			statusBar.setText (String.format ("Kom inn på panelet på [%d, %d]", me.getX(), me.getY())); 
		}
		
		public void mouseExited (MouseEvent me) {
			statusBar.setText (String.format ("Forsvant fra panelet på [%d, %d]", me.getX(), me.getY())); 
		}
		
		// MouseMotionListener
		public void mouseDragged (MouseEvent me) {
			statusBar.setText (String.format ("MouseDragget til [%d, %d]", me.getX(), me.getY()));
		}
		
		public void mouseMoved (MouseEvent me) {
			statusBar.setText (String.format ("MouseMoved til [%d, %d]", me.getX(), me.getY()));
		} 
	}
}