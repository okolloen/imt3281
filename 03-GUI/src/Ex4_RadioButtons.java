import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex4_RadioButtons extends JFrame {
	private Font fontPlain, fontItalic, fontBold, fontBoldItalic;
	private JRadioButton plain, italic, bold, boldItalic; 
	private ButtonGroup fontSelector;
	private JTextField text;
	
	public Ex4_RadioButtons () {
		super ("Radio knapper velger font");
		setLayout (new FlowLayout ());
		text = new JTextField ("Se fonten endre seg", 20); 
		add (text);
		fontPlain = new Font ("Serif", Font.PLAIN, 14);
		fontItalic = new Font ("Serif", Font.ITALIC, 14);
		fontBold = new Font ("Serif", Font.BOLD, 14);
		fontBoldItalic = new Font ("Serif", Font.ITALIC+Font.BOLD, 14); 
		
		text.setFont (fontPlain);

		plain = new JRadioButton ("Plain", true);
		italic = new JRadioButton ("Italic", false);
		bold = new JRadioButton ("Bold", false);
		boldItalic = new JRadioButton ("Bold/Italic", false); 
		add (plain);
		add (italic); 
		add (bold);
		add (boldItalic);
		
		fontSelector = new ButtonGroup (); 
		fontSelector.add (plain); 
		fontSelector.add (italic); 
		fontSelector.add (bold); 
		fontSelector.add (boldItalic);
		
		plain.addItemListener (new FontSelectorHandler (fontPlain)); 
		italic.addItemListener (new FontSelectorHandler (fontItalic)); 
		bold.addItemListener (new FontSelectorHandler (fontBold)); 
		boldItalic.addItemListener (new FontSelectorHandler (fontBoldItalic));
	}
	
	private class FontSelectorHandler implements ItemListener {
		Font f;
		
		public FontSelectorHandler (Font f) { 
			this.f = f;
		}
		
		public void itemStateChanged (ItemEvent ie) { 
			if (ie.getStateChange()==ItemEvent.SELECTED)
				text.setFont (f); 
		}
	}
}