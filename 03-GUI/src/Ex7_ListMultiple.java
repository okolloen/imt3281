import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
import javax.swing.event.*;

public class Ex7_ListMultiple extends JFrame {
	private JList colorList, copyList;
	private JButton copyButton;
	private static final String colorNames[] = { "Black", "Blue", "Cyan", "Dark grey", "Grey",
												 "Green", "Light grey", "Magenta", "Orange", 
												 "Pink", "Red", "White", "Yellow" };
	
	public Ex7_ListMultiple () {
		super ("Multiple selection list"); 
		setLayout (new FlowLayout ());
		colorList = new JList (colorNames);
		colorList.setVisibleRowCount (5); 
		colorList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); 
		add (new JScrollPane (colorList));
		
		copyButton = new JButton ("Kopier >>");
		copyButton.addActionListener (new CopyAction ());
		add (copyButton);
		
		copyList = new JList ();
		copyList.setVisibleRowCount (5);
		copyList.setFixedCellWidth (100);
		copyList.setFixedCellHeight (15); 
		copyList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION); 
		add (new JScrollPane (copyList));
	}
	
	private class CopyAction implements ActionListener { 
		public void actionPerformed (ActionEvent ae) {
			copyList.setListData (colorList.getSelectedValues ()); }
	} 
}