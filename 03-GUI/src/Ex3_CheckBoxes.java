import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 * Etter eksempel 14.17 i læreboka
 */
public class Ex3_CheckBoxes extends JFrame { 
	private JTextField text;
	private JCheckBox bold, italic;

	public Ex3_CheckBoxes () {
		super ("JCheckBox test");
		setLayout (new FlowLayout ());
		text = new JTextField ("Se teksten forandre seg", 15);
		text.setFont (new Font ("Serif", Font.PLAIN, 20)); 
		add (text);
		bold = new JCheckBox ("Bold"); 
		italic = new JCheckBox ("Italic"); 
		add (bold);
		add (italic);
		
		CheckBoxHandler cbh = new CheckBoxHandler (); 
		bold.addItemListener (cbh); 
		italic.addItemListener (cbh);
	}
	
	class CheckBoxHandler implements ItemListener { 
		
		public void itemStateChanged (ItemEvent ie) {
			if (bold.isSelected() && italic.isSelected())
				text.setFont (new Font ("Serif", Font.BOLD+Font.ITALIC, 20));
			else if (bold.isSelected())
				text.setFont (new Font ("Serif", Font.BOLD, 20));
			else if (italic.isSelected())
				text.setFont (new Font ("Serif", Font.ITALIC, 20));
			else
				text.setFont (new Font ("Serif", Font.PLAIN, 20));
		} 
	}
}