import java.awt.*;
import javax.swing.*; 
import javax.swing.event.*;

public class Ex6_List extends JFrame {
	private JList colorList;
	private static final String colorNames[] = { "Black", "Blue", "Cyan", "Dark grey", "Grey",
												 "Green", "Light grey", "Magenta", "Orange", 
												 "Pink", "Red", "White", "Yellow" }; 
	private static final Color colors[] = { Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY, 
											Color.GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA, 
											Color.ORANGE, Color.PINK , Color.RED, Color.WHITE, Color.YELLOW };
	
	public Ex6_List () {
		super ("Test av JList"); 
		setLayout (new FlowLayout ());
		colorList = new JList (colorNames);
		colorList.setVisibleRowCount (5);
		colorList.setSelectionMode (ListSelectionModel.SINGLE_SELECTION); 
		colorList.addListSelectionListener (new ListSelection ());
		add (new JScrollPane (colorList)); 
	}
	
	private class ListSelection implements ListSelectionListener {
		public void valueChanged (ListSelectionEvent lse) { 
			if (!lse.getValueIsAdjusting())
				getContentPane().setBackground (colors[colorList.getSelectedIndex()]); 
		}
	} 
}