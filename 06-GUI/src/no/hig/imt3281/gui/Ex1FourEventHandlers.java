package no.hig.imt3281.gui;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Ex1FourEventHandlers extends JFrame implements ActionListener {
	JButton button1, button2;
	JTextField tField1;
	
	public Ex1FourEventHandlers() {
		super ("Showing the four ways to implement event handlers");
		setLayout (new FlowLayout());
		JPanel buttons = new JPanel (new GridLayout(1, 2));
		buttons.add(button1 = new JButton ("Open new window"));
		button1.addActionListener (new WindowMagic());
		buttons.add(button2 = new JButton ("Change textfield"));
		button2.addActionListener (this);
		add (buttons);
		add (tField1 = new JTextField("Initial text in textfield", 40));
		tField1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((JTextField)e.getSource()).setText("You pressed 'enter'!");
			}
		});
		addWindowListener (new WindowClosing ());
		setSize(1000, 1000);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		tField1.setText("When done, close the window");
	}

	class WindowClosing extends WindowAdapter {
		@Override
		public void windowClosing (WindowEvent we) {
			if (JOptionPane.OK_OPTION==JOptionPane.showConfirmDialog(Ex1FourEventHandlers.this, 
																							"Are you sure you want to close this window?"))
				System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		new Ex1FourEventHandlers();
	}
}
