package no.hig.imt3281.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class WindowMagic extends JFrame implements ActionListener {
	
	public WindowMagic() {
		super ("A whole new window");
		add (new JLabel(new ImageIcon(getClass().getResource("/images/ludo.png"))));
		pack ();
		setLocation(100, 100);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		setVisible(true);
	}
}
