package no.hig.imt3281.generics;

//Old, not used example
//See textbook chapter 21 (21.6-21.8)
public class GenericStackTest {
	public static void main(String[] args) {
		Double[] doubleElements = { 1.1, 2.2, 3.3, 4.4, 5.5 };
		Integer[] intElements = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		
		Stack<Double> doubleStack = new Stack<Double>(5);
		Stack<Integer> intStack = new Stack<Integer>();
		
		testPush (doubleStack, doubleElements);
		testPop (doubleStack);
		
		testPush (intStack, intElements);
		testPop (intStack);
	}

	private static <T> void testPush(Stack<T> stack, T[] elements) {
		System.out.println ("\nPushing elements onto Stack");
		for (T value : elements) {
			System.out.printf ("%s ", value);
			stack.push (value);
		}
	}

	private static <T> void testPop(Stack<T> stack) {
		try {
			System.out.println("\nPopping elements from Stack");
			T popValue;
			while (true) {
				popValue = stack.pop();
				System.out.printf ("%s ", popValue);
			}
		} catch (EmptyStackException ese) {
			ese.printStackTrace();
		}
	}
}
