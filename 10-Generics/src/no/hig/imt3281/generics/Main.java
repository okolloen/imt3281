package no.hig.imt3281.generics;

public class Main {
	public static String key1 = "This is the first key";
	public static Integer value1 = 5;

	static Integer iarr[] = {1, 2, 3, 4};
	static Character carr[] = {'g', 'r', 'e', 'a', 't'};
	
	public static NoGeneric noGen = new NoGeneric (key1, value1);
	public static IsGeneric<String, Integer> isGen = new IsGeneric<> (key1, value1);

	private static <T> void printMe (T args[]) {
		for (T b : args) {
			System.out.printf("%s ", b);
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		printMe (iarr);
		printMe (carr);
		
		Integer int1 = (Integer)noGen.getValue();
		// String val = (String)noGen.getValue();
		String string1 = isGen.getKey();
	}

}
