package no.hig.imt3281.generics;

// Old, not used example
// See textbook chapter 21 (21.6-21.8)
@SuppressWarnings("serial")
public class EmptyStackException extends RuntimeException {
	public EmptyStackException () {
		this ("Ingen elementer på stakken");
	}
	
	public EmptyStackException (String msg) {
		super (msg);
	}
}
