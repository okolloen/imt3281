package no.hig.imt3281.generics;

import java.util.ArrayList;

class Machine {
	public String toString() {
		return "I am a machine";
	}
	
	public void start () {
		System.out.println("Starting");
	}
}

class ThreeDPrinter extends Machine {
	
}

public class Wildcard {
	
	public static void main(String[] args) {
		ArrayList<Machine> list1 = new ArrayList<Machine>();
		list1.add(new Machine());
		list1.add(new Machine());
		
		ArrayList<ThreeDPrinter> list2 = new ArrayList<>();
		list2.add(new ThreeDPrinter());
		list2.add(new ThreeDPrinter());
		
		showList (list2);
		showMachineList (list2);
	}

	public static void showList(ArrayList<?> list) {
		for (Object o : list)
			System.out.println(o);
	}
	
	public static void showMachineList(ArrayList<? extends Machine> list) {
		for (Machine m : list)
			m.start();
	}
}