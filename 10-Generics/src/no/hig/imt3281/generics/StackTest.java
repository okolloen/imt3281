package no.hig.imt3281.generics;

//Old, not used example
//See textbook chapter 21 (21.6-21.8)
public class StackTest {
	public static void main(String[] args) {
		double[] doubleElements = { 1.1, 2.2, 3.3, 4.4, 5.5 };
		int[] intElements = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		
		Stack<Double> doubleStack = new Stack<Double>(5);
		Stack<Integer> intStack = new Stack<Integer>();
		
		testPushDouble (doubleStack, doubleElements);
		testPopDouble (doubleStack);
		
		testPushInteger (intStack, intElements);
		testPopInteger (intStack);
	}

	private static void testPushDouble(Stack<Double> doubleStack,
			double[] doubleElements) {
		System.out.println ("\nPushing elements onto doubleStack");
		for (double value : doubleElements) {
			System.out.printf ("%.1f ", value);
			doubleStack.push (value);
		}
	}

	private static void testPopDouble(Stack<Double> doubleStack) {
		try {
			System.out.println("\nPopping elements from doubleStack");
			double popValue;
			while (true) {
				popValue = doubleStack.pop();
				System.out.printf ("%.1f ", popValue);
			}
		} catch (EmptyStackException ese) {
			ese.printStackTrace();
		}
	}

	private static void testPushInteger(Stack<Integer> intStack,
			int[] intElements) {
		System.out.println ("\nPushing elements onto intStack");
		for (int value : intElements) {
			System.out.printf ("%d ", value);
			intStack.push (value);
		}
	}

	private static void testPopInteger(Stack<Integer> intStack) {
		try {
			System.out.println("\nPopping elements from intStack");
			int popValue;
			while (true) {
				popValue = intStack.pop();
				System.out.printf ("%d ", popValue);
			}
		} catch (EmptyStackException ese) {
			ese.printStackTrace();
		}
	}
}
