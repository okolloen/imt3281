package no.hig.imt3281.generics;

public class NoGeneric {
	private Object key;
	private Object value;
	
	public NoGeneric (Object key, Object value) {
		this.key = key;
		this.value = value;
	}
	
	public Object getKey () {
		return key;
	}
	
	public Object getValue () {
		return value;
	}
}
