package no.hig.imt3281.generics;

import java.util.ArrayList;

//Old, not used example
//See textbook chapter 21 (21.6-21.8)
public class Stack <T>{
	private ArrayList<T> elements;
	
	public Stack() {
		this (10);
	}
	
	public Stack(int capacity) {
		int initCapacity = capacity>0?capacity:10;
		elements = new ArrayList<T>(initCapacity);
	}
	
	public void push (T pushValue) {
		elements.add(pushValue);
	}
	
	public T pop () {
		if (elements.isEmpty())
			throw new EmptyStackException ("Ingen flere elementer på stacken.");
		return elements.remove(elements.size()-1);
	}
}
