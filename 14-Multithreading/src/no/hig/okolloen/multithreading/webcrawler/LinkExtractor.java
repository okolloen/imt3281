package no.hig.okolloen.multithreading.webcrawler;

import java.awt.GridLayout;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/*
 * Parses a HTML page and extract links from that page. 
 * ParsingObjects is fetched from a queue and the content of the page is
 * parsed and all links found. A list of all previously found pages is also kept so that we won't 
 * check the same page twice. When a new URL is found it is placed in a queue to be read by a WebReader.
 * We also keep track of the number of pages that has been parsed. 
 * 
 * Visually this is shown as a panel with status for parse queue length, pages found and pages parsed.
 */
@SuppressWarnings("serial")
public class LinkExtractor extends JPanel implements Runnable {
	// As new URLs are found they are placed in this queue to be read
	private LinkedBlockingQueue<URL> pagesToRead;
	// As pages are read they are placed here to be parsed
	private LinkedBlockingQueue<ParsingObjects> pagesToParse;
	// We need to keep a complete list of all pages found up to now
	// This to avoid to check the same pages over and over again
	private LinkedList<URL> pagesFound;
	// Keep a running count of the number of pages parsed
	private static int pagesParsed = 0;
	private JLabel parseQueLength;
	private JLabel numberOfPagesFound;
	private JLabel numberOfPagesParsed;

	/*
	 * Store references to the two queues and the list of urls found, then set up the GUI components
	 * 
	 * @param input the queue to fetch pages for parsing from.
	 * @param output the queue to place URLs to be read on.
	 * @param pagesFound a list of URLs found up to now
	 */
	public LinkExtractor (LinkedBlockingQueue<ParsingObjects> input, LinkedBlockingQueue<URL> pagesToRead, LinkedList<URL> pagesFound) {
		pagesToParse = input;
		this.pagesToRead = pagesToRead;
		this.pagesFound = pagesFound;
		// A simple two column by three rows layout
		setLayout (new GridLayout(3, 2));
		JLabel queLength = new JLabel("Parse que length: ", JLabel.RIGHT);
		add (queLength);
		parseQueLength = new JLabel ("0");
		add (parseQueLength);
		JLabel numberFound = new JLabel("Pages found: ", JLabel.RIGHT);
		add (numberFound);
		numberOfPagesFound = new JLabel ("0");
		add (numberOfPagesFound);
		JLabel numberParsed = new JLabel("Pages parsed: ", JLabel.RIGHT);
		add (numberParsed);
		numberOfPagesParsed = new JLabel ("0");
		add (numberOfPagesParsed);
	}
	
	/*
	 * Parsing happens here.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		ParsingObjects toParse;
		try {
			// Fetch a page to parse
			while ((toParse = pagesToParse.take())!=null) {	// This never return null, so this is an eternal loop
				// Fetch the content of the page
				String html = toParse.getContent();
				// and the URL that it was read from
				URL startingPage = toParse.getSource();
				int idx1 = 0;
				String newURLString;
				// Search for the next occurence of "href=", this is alway a link to something or other
				while ((idx1 = html.indexOf("href=", idx1))>-1) {
					// Did the author of the page use " or ' around their URLs???
					if ((html.indexOf("'", idx1)==-1)||(html.indexOf("\"", idx1)<html.indexOf("'", idx1))) {
						// OK, " is used, find the text between the first and second "
						idx1 = html.indexOf("\"", idx1);
						newURLString = html.substring (idx1+1, html.indexOf("\"", idx1+1));
					} else {
						// ' was used, find the text between the first and secon '
						idx1 = html.indexOf("'", idx1);
						newURLString = html.substring (idx1+1, html.indexOf("'", idx1+1));
					}
					try {
						// Create a new URL, this way it works both with absolute and relative URLs
						URL newURL = new URL (startingPage, newURLString);
						if (!pagesFound.contains(newURL)) {	// We found a brand new URL
							pagesFound.add(newURL);			// Add it to the pagesFound list
							pagesToRead.offer(newURL);		// and to the queue of pages to be read
						}
					} catch (MalformedURLException murle) {
						System.err.printf ("Bad URL : %s\n", newURLString);
					}
				}
				// This means that one more page has been completely parsed
				pagesParsed++;
				// Again, we need to make sure that we update these in a timely manner
				// place it in the GUI event dispatch queue
				SwingUtilities.invokeLater(new Thread () {
					public void run () {
						parseQueLength.setText(Integer.toString(pagesToParse.size()));
						numberOfPagesFound.setText(Integer.toString(pagesFound.size()));
						numberOfPagesParsed.setText(Integer.toString(pagesParsed));
					}
				});
			}
		} catch (InterruptedException ie) {
			// Ouch, this is to bad, since we only have one such thread. 
			// Should really not happen.
			System.err.println ("Link extractor thread is dying.............");
			ie.printStackTrace();
		}
	}
}
