package no.hig.okolloen.multithreading.webcrawler.v2;

import java.net.URL;

/*
 * Stores a page and it's URL, ready to be parsed.
 * We need to know the URL from which content was read when parsing the page to be able to
 * work with relative URL's. 
 */
public class ParsingObjects {
	URL source;
	String content;
	
	public ParsingObjects (URL u, String content) {
		source = u;
		this.content = content;
	}

	public URL getSource() {
		return source;
	}

	public void setSource(URL source) {
		this.source = source;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
