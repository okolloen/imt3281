package no.hig.okolloen.multithreading.webcrawler.v2;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownServiceException;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/*
 * Objects of this class is used to read contents of URLs.
 * URLs to be read is fetched from a queue and the resulting content is
 * placed into another queue to be parsed and checked for new URLs to read.
 * 
 * Visually it is a panel with the filename, a status message and a progress bar.
 * The filename being read is displayed. The status message is either "Initializing" or
 * the size of the file that is being read. The progress bar is indeterminate at all other times than 
 * when actual reading is performed.
 */
@SuppressWarnings("serial")
public class WebReader extends JPanel implements Runnable{
	// A reference to the queue holding URLs to be read
	LinkedBlockingQueue<URL> pagesToRead;
	// A reference to the queue where content that is read is to be placed
	LinkedBlockingQueue<ParsingObjects> pagesToParse;
	// The elements that is placed in the panel
	JLabel fname;
	JProgressBar progress;
	JLabel fsize;
	int sizeRead = 0;
	public static BufferedWriter fnfLog;	
	
	/*
	 * The constructor stores the input and output queues and set up the display.
	 * 
	 * @param input a linkedBlockingQueue containing the URLs to be read. This queue will be added to
	 * by the thread that parses the content which is read by threads of this class.
	 * 
	 * @param output a LinkedBlockingQueue or ParsingObjects, a ParsingObject contains the URL and the content of 
	 * that same URL. IE it what is produced by this class.
	 */
	public WebReader (LinkedBlockingQueue<URL> input, LinkedBlockingQueue<ParsingObjects> output) {
		// Store a reference to input and output
		pagesToRead = input;
		pagesToParse = output;
		// Set up the filename label
		add (fname = new JLabel ("                       "));
		// Override the size of the label
		fname.setPreferredSize(new Dimension(350, 15));
		// Set up the status label
		add (fsize = new JLabel ("00000000b"));
		// Override the size of this label
		fsize.setPreferredSize(new Dimension(100, 15));
		// Sett up the progress bar
		add (progress = new JProgressBar(JProgressBar.HORIZONTAL));
		// Override the size of the progress bar
		progress.setPreferredSize(new Dimension(200, 15));
	}
	
	/*
	 * All reading from the web happens here.
	 * This code is being executed in a separate thread for each instance of this class.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run () {
		URL toRead;
		try {
			// Get an URL to read from the buffer
			while ((toRead = pagesToRead.take())!=null) {	// This should never return null so this is in effect an eternal loop
				try {
					// We get the filename to read from the URL
					fname.setText(toRead.getPath());
					Thread.sleep(100);
					// Initializing, set the progress to indeterminate since we don't know anything yet
					progress.setIndeterminate(true);
					// Let the user know that we are starting up this download
					fsize.setText("Initializing");
					// Opens the connection on this URL
					URLConnection con = toRead.openConnection();
					// Unable to get a connection, continue with next item.
					if (con==null)
						continue;
					// Get the length from the connection (This and the next step requires that the 
					// header information from the server is read
					int length = con.getContentLength();
					// Get the content type from the connection
					String contentType = con.getContentType();
					// No content type, makes it impossible to continue with this URL.
					if (contentType==null)
						continue;
					// If this is not a html file we throw it away and fetch the next one
					if (!contentType.startsWith("text/html"))
						continue;
					// Set the size of the file to what we found from the connection
					fsize.setText(String.format("%3.1fKb", length/1024.0f));
					// If we get an undetermined file length it is reported as -1
					// Setting it as 1MB in that case (just to have something.)
					try {
						progress.setMaximum(length>-1?length:1024*1024);
						// We haven't downloaded anything yet
						progress.setValue(0);
						// Now we are ready to start downloading
						progress.setIndeterminate(false);
					} catch (NullPointerException npe) {
						
					}
					// Creating a bufferedreader on top of a inputstreamreader on top of the actuall stream from the URL
					BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
					// We use a StringBuilder to add all the string we read from the URL
					StringBuilder sb = new StringBuilder();
					// Read strings into tmp
					String tmp;
					// Currently we have read 0 bytes
					sizeRead = 0;
					while ((tmp = br.readLine())!=null) { // Loop until eof
						// Add the string to the stringbuilder, no need to add linefeeds as we will just search for a-tags
						sb.append (tmp);
						// We have read stringlength+1 bytes (approximately)
						sizeRead += tmp.length()+1;
						// Need to update the value of the progress bar, but this should really happen on the GUI thread
						// If not it might mess up everything!!!
						// SwingUtilities.invokelater places a task in the GUI event queue to be processed in the GUI thread
						SwingUtilities.invokeLater(new Thread() {
							public void run () {
								progress.setValue(sizeRead);
							}
						});
					}
					// We are done reading, indicate as completed
					sizeRead = progress.getMaximum();
					// Again, we update the progress bar on the GUI thread
					SwingUtilities.invokeLater(new Thread() {
						public void run () {
							progress.setValue(sizeRead);
						}
					});
					// Create a ParsingObjects object with the content and the URL from which it was read
					ParsingObjects newPage = new ParsingObjects(toRead, sb.toString());
					// Put the new item on the queue to be parsed
					pagesToParse.offer(newPage);
				} catch (FileNotFoundException fnfe) {
					synchronized (fnfLog)  {
						try {
							fnfLog.write (fnfe.getMessage());
							fnfLog.newLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (UnknownServiceException use) {
				} catch (IOException ioe) {
					synchronized (fnfLog)  {
						try {
							fnfLog.write (ioe.getMessage());
							fnfLog.newLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (fnfLog!=null) {
				try {
					fnfLog.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fnfLog = null;
			}
		}
	}
}
