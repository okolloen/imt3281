package no.hig.okolloen.multithreading.webcrawler;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * Starts at http://www.hig.no/ and will given enough time read everything on the Internet that is directly
 * or indirectly linked to from that place.
 * 
 * A big task, but it shows multithreading in practice.
 */
@SuppressWarnings("serial")
public class Crawler extends JFrame {
	// A bunch of readers, since reading from the web will be the slowest thing here, involving a lot of waiting for the network
	private WebReader readers[] = new WebReader[15];
	// A single page parser can keep up with a lof of readers
	private LinkExtractor pageParser;
	// One queue for the pages to be read
	private LinkedBlockingQueue<URL> pagesToRead;
	// and one queue for the pages that is read and are ready to be parsed
	private LinkedBlockingQueue<ParsingObjects> pagesToParse; 
	// A list of all pages found 
	private LinkedList<URL> pagesFound = new LinkedList<URL>();
	
	/*
	 * The constructor set up the GUI and starts all the threads.
	 */
	public Crawler () {
		super ("Web crawler");
		// A queue of pages to read, of unlimited size
		// This works since it only holds URLs
		pagesToRead = new LinkedBlockingQueue<URL>();
		// A queue of pages to parse, we do not want more than
		// fifty pages in this queue since it contains the complete HTML for the pages 
		pagesToParse = new LinkedBlockingQueue<ParsingObjects>(50);
		
		// Create a pool of threads
		ExecutorService application = Executors.newCachedThreadPool();
		// Adding web readers
		// Put them in a panel and start them up
		JPanel crawlers = new JPanel ();
		// Add a border with a title around the panel with the readers
		crawlers.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Reading threads"));
		// One column, as many rows as is needed
		crawlers.setLayout(new GridLayout(0, 1));
		// Go through each reader in the array 
		for (WebReader reader : readers) {
			// Create the reader object
			reader = new WebReader(pagesToRead, pagesToParse);
			// Add it to the layout
			crawlers.add (reader);
			// And start it up
			application.execute(reader);
		}
		add (crawlers, BorderLayout.CENTER);
		// Adding a link extractor thread
		// Put it in a panel and start it up
		pageParser = new LinkExtractor(pagesToParse, pagesToRead, pagesFound);
		// Add a nice border around the panel with an explanatory title in it
		pageParser.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Page parser thread"));
		// Add the parser panel to the layout
		add (pageParser, BorderLayout.SOUTH);
		// start up the parsing thread
		application.execute(pageParser);
		// Pack the window, ie, give it the needed size
		pack ();
		// When the window closes, force all threads to shut down and terminate the program
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Display the window
		setVisible (true);
		try {
			// Add the starting URL to the list of pages found
			pagesFound.add(new URL ("http://www.hig.no/"));
			// Add the starting URL to the list of URLs to be read
			// this will give the first reader object something to do, then things will start
			// to get chaotic :)
			pagesToRead.offer(new URL ("http://www.hig.no/"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Well ;)
		new Crawler ();
	}

}
