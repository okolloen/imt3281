package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Test the blocking buffer, this test should always yield the correct result
 */
public class Eks4_BlockingBufferTest {
	public static void main(String[] args) {
		// Set up a thread pool
		ExecutorService application = Executors.newCachedThreadPool();
		
		// Create a BlockingBuffer
		Buffer sharedLocation = new BlockingBuffer();
		
		// Start the producer and consumer threads utilizing the blocking buffer
		application.execute(new Producer(sharedLocation));
		application.execute(new Consumer(sharedLocation));
		
		// Shut down the thread pool once the threads terminate 
		application.shutdown();
	}
}
