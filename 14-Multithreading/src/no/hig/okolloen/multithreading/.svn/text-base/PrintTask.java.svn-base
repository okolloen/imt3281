package no.hig.okolloen.multithreading;

import java.util.Random;

/*
 * By implementing the runnable interface (or extending the Thread class)
 * you create something that can be run in a separate thread.
 * 
 * The code that is to be run in a separate thread goes in the run method.
 */
public class PrintTask implements Runnable {
	private final int sleepTime;
	private final String taskName;
	private final static Random generator = new Random();
	
	/*
	 * Constructor for the printing task, takes the name of the process as a parameter
	 */
	public PrintTask (String name) {
		// The name of the process
		taskName = name;
		// The amount of time to sleep, between 0 and five seconds
		sleepTime = generator.nextInt(5000);
	}
	
	/*
	 * This method contains the code that will be executed in a separate thread
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			// Gives a message on the console
			System.out.printf("%s going to sleep for %d milliseconds.\n", taskName, sleepTime);
			// Then suspends the thread for the given amount of time
			Thread.sleep(sleepTime);
		} catch (InterruptedException ie) {
			// If the thread for some reason is awakened prematurely
			System.out.printf("%s terminated prematurely due to interruption.\n", taskName);
		}
		// We are done sleeping, all done and terminating this thread
		// by exiting the run method.
		System.out.printf ("%s done sleeping.\n", taskName);
	}
}