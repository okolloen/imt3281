package no.hig.okolloen.multithreading;

import java.util.concurrent.ArrayBlockingQueue;

/*
 * A Thread safe buffer, utilizing an ArrayBlockingQueue to achieve the thread safety
 */
public class BlockingBuffer implements Buffer {
	// An ArrayBlockingQueue is inherently thread safe.
	// Can be of a fixed size, once full any thread that wish to add to the queue
	// will be put on hold until space is again available. Same thing for threads
	// fetching data from the queue, if the queue is empty the thread is suspended
	// until data becomes available
	private final ArrayBlockingQueue<Integer> buffer;
	
	/*
	 * The constructor initializes the queue to be available to store one item
	 */
	public BlockingBuffer () {
		// The size of the buffer is ONE item
		buffer = new ArrayBlockingQueue<Integer>(1);
	}

	/*
	 * Enters a value into the buffer, will suspend the thread until the buffer is emptied
	 * if non empty. 
	 * 
	 * Also gives a status message of what is going on and how much of the buffer is occupied.
	 * 
	 * @param value the value to be entered into the buffer
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#set(int)
	 */
	@Override
	public void set(int value) throws InterruptedException {
		// Put a value into the buffer, suspend the thread if the buffer is none empty.
		buffer.put(value);
		System.out.printf("%s%2d\t%s%d\n", "Producer writes ", value, "Buffer cells occupied: ", buffer.size());

	}

	/*
	 * Fetch a value from the buffer, if the buffer is empty the thread will be suspended until a value becomes available.
	 * 
	 * @return the value of the buffer.
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#get()
	 */
	@Override
	public int get() throws InterruptedException {
		// Get a value from the buffer, suspend the thread if the buffer is empty.
		int readValue = buffer.take();
		System.out.printf ("%s %2d\t%s%d\n", "Consumer reads ", readValue, "Buffer cells occupied: ", buffer.size());
		return readValue;
	}
}