package no.hig.okolloen.multithreading;

/*
 * Creating our own synchronized buffer using the keyword synchronized
 */
public class SynchronizedBuffer implements Buffer {
	// Our buffer can hold one value
	private int buffer = -1;
	// Boolean flag indicating whether or not the buffer is occupied
	private boolean occupied = false;

	/*
	 * The keyword synchronized means that the running thread acquires the locking mechanisms so that we 
	 * can suspend this thread and awaken other threads accessing this object.
	 * 
	 * @param value the value to enter into the buffer
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#set(int)
	 */
	@Override
	public synchronized void set(int value) throws InterruptedException {
		// Loop for as long as the buffer is occupied.
		while (occupied) {
			// Give a status message
			System.out.println("Producer tries to write.");
			// and some more detailed information
			displayState ("Buffer full, Producer waits.");
			// Suspends this thread, it will sleep until awakened by a call to notify
			wait ();
		}
		
		// Set the value of the buffer
		buffer = value;
		// The buffer is now occupied, no further writing is possible
		occupied = true;
		// Display detailed information about the buffer
		displayState ("Producer writes "+buffer);
		// Notify all waiting threads, if more threads is waiting to read/write they will be awakened
		notifyAll();
	}

	/*
	 * Same thing with the synchronized as with the set method.
	 * 
	 * @return the value in the buffer
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#get()
	 */
	@Override
	public synchronized int get() throws InterruptedException {
		// Loop for as long as there is nothing in the buffer
		while (!occupied) {
			// Give a status message about what happens
			System.out.println("Consumer tries to read.");
			// And some more detailed information
			displayState ("Buffer empty, Consumer waits.");
			// Suspend the thread, it will sleep until awakened by a call to notify
			wait ();
		}
		// The buffer is no longer holding any data
		occupied = false;
		// Display some more details about the buffer
		displayState ("Consumer reads "+buffer);
		// Awaken all waiting threads
		notifyAll();
		// Return the value of the buffer
		return buffer;
	}

	// Display more detailed information about this buffer
	private void displayState(String operation) {
		System.out.printf ("%-40s%d\t\t%b\n\n", operation, buffer, occupied);
	}
}
