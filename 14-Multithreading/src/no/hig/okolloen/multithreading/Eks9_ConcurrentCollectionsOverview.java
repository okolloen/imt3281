package no.hig.okolloen.multithreading;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/*
 * A quick browser to view the Concurrent collections framework for Java 1.6/1.7
 */
@SuppressWarnings("serial")
public class Eks9_ConcurrentCollectionsOverview extends JFrame implements HyperlinkListener {
	JEditorPane html = new JEditorPane ();
	JButton onePointSix = new JButton ("Java 1.6");
	JButton onePointSeven = new JButton ("Java 1.7");
	
	public Eks9_ConcurrentCollectionsOverview () {
		super ("Overview of the Concurrent Collections framework");
		JPanel top = new JPanel ();
		top.add (onePointSix);
		onePointSix.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					html.setPage(new URL("http://download.oracle.com/javase/6/docs/api/java/util/concurrent/package-summary.html"));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		top.add (onePointSeven);
		onePointSeven.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					html.setPage(new URL("http://download.oracle.com/javase/7/docs/api/java/util/concurrent/package-summary.html"));
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		add (top, BorderLayout.NORTH);
		add (new JScrollPane (html));
		html.setEditable(false);
        html.addHyperlinkListener(this);
		Dimension screenSize = getToolkit().getScreenSize();
	    int width = screenSize.width;
	    int height = screenSize.height;
	    setBounds(0, 0, width, height);
	    setVisible(true);
	    onePointSix.doClick();
	}

	  /*
	   * N�r brukeren trykker p� en link i "nettleseren" v�r s� blir denne metoden kallt
	   * @see javax.swing.event.HyperlinkListener#hyperlinkUpdate(javax.swing.event.HyperlinkEvent)
	   */
	  public void hyperlinkUpdate(HyperlinkEvent event) {
		// Dette betyr at vi skal g� til en ny side
	    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	      try {
	    	// Vi laster inn den nye siden, event.getURL henter adressen som en URL
	        html.setPage(event.getURL());
	      } catch(IOException ioe) {
	        // Gir en feilmelding dersom det er problemer med � laste siden
	        ioe.printStackTrace();
	      }
	    }
	  }
	
	public static void main(String[] args) {
		new Eks9_ConcurrentCollectionsOverview();
	}
}
