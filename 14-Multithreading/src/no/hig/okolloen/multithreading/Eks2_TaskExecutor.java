package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * The new, fancy way of doing multithreading.
 * The threads is kept in a pool and reused.
 * This way we avoid creating to many threads (not swamping the system with threads.) 
 * Threads is also reused and we avoid the costly process of creating and removing threads.
 */
public class Eks2_TaskExecutor {
	public static void main(String[] args) {
		// No need to encapsulate the runnable objects in Thread objects
		PrintTask task1 = new PrintTask ("Task 1");
		PrintTask task2 = new PrintTask ("Task 2");
		PrintTask task3 = new PrintTask ("Task 3");
		
		System.out.println ("Tasks created, starting Executor.");
		
		// Getting a cached pool of threads
		ExecutorService threadExecutor = Executors.newCachedThreadPool();
		
		System.out.println("Starting tasks...");
		
		// Execute all three threads
		threadExecutor.execute(task1);
		threadExecutor.execute(task2);
		threadExecutor.execute(task3);
		
		// Shutting down the thread pool, but only when all threads have finished
		threadExecutor.shutdown();
		
		// Sends a message to the console that all threads have been started
		System.out.println ("Tasks started, ending main.\n");
		// Note, the program will not terminate before all running threads have terminated.
	}
}

