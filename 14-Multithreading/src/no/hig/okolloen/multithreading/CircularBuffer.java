package no.hig.okolloen.multithreading;

/*
 * Creating a buffer with room for more than one item at a time.
 * This buffer could have been created much simpler by utilizing an ArrayBlockingQueue
 * or similar, but this shows what is really going on.
 */
public class CircularBuffer implements Buffer {
	// We will have room for four items in this buffer
	private final int[] buffer = { -1, -1, -1, -1 };
	// Keep tab of how many cells is currently occupied
	private int occupiedCells = 0;
	// We should write to element 0, 1, 2, 3, 0, 1, 2, .... 
	private int writeIndex = 0;
	// And read from element 0, 1, 2, 3, 0, 1, 2, ..... 
	private int readIndex = 0;
	
	/*
	 * Set a value into the buffer if there is room for more, if not suspend the thread until room becomes available.
	 * We can set up to four values without reading, but then we would have to wait for a read operation before being
	 * able to write any more items into the buffer.
	 * 
	 * @param value the value to enter into the buffer
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#set(int)
	 */
	@Override
	public synchronized void set(int value) throws InterruptedException {
		while (occupiedCells==buffer.length) {	// if the buffer is full we need to wait
			System.out.println("Buffer is full. Producer waits.");
			// Suspend the thread until we receive a notify
			wait ();
		}
		
		// Put the value into the next available slot in the buffer
		buffer[writeIndex] = value;
		// Move the write index ahead, wrap back to 0 if at the end of the buffer
		writeIndex = (writeIndex+1)%buffer.length;
		// Now we have one more value in the buffer
		occupiedCells++;
		// Give an overview of the state of the buffer
		displayState ("Producer writes "+value);
		// Notify all waiting threads
		notifyAll();
	}

	/*
	 * Fetch a value from the buffer, the buffer can hold up to four values simultaneously but we fetch one at a time.
	 * If no value is available, suspend the thread until a value becomes available.
	 * 
	 * @return the value from the buffer.
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#get()
	 */
	@Override
	public synchronized int get() throws InterruptedException {
		while (occupiedCells==0) {	// If no value available the we wait
			System.out.println("Buffer is empty. Consumer waits.");
			// Suspend the thread until we receive a notify
			wait ();
		}
		// Get the value from the buffer
		int readValue = buffer[readIndex];
		// Move the read index up one position, wrap it back to 0 if at the end of the buffer
		readIndex = (readIndex+1)%buffer.length;
		// Now one less item is available in the buffer
		--occupiedCells;
		// Give and overview of the buffer status
		displayState ("Consumer reads "+readValue);
		// Notify all suspended threads
		notifyAll();
		// Return the value from the buffer
		return readValue;
	}

	/*
	 * Used to give an overview of the state of the buffer
	 * Shows details about the buffer contents and the read/write indexes
	 * 
	 * @param operation a string explaining what is currently happening
	 */
	public void displayState (String operation) {
		// Give a short intro, with the current operation and the number of buffer cells occupied
		System.out.printf("%s%s%d)\n%s", operation, " (buffer cells occupied: ", occupiedCells, "buffer cells: ");
		// Write out the contents of the buffer cells
		for (int value : buffer)
			System.out.printf(" %2d  ", value);
		
		// Write out a bit of separation marks
		System.out.print("\n              ");
		for (int i=0; i<buffer.length; i++)
			System.out.print("---- ");
		// Write out the r/w indexes
		System.out.print("\n              ");
		for (int i=0; i<buffer.length; i++) {
			if (i==writeIndex && i == readIndex)
				System.out.print(" WR  ");
			else if (i==writeIndex)
				System.out.print(" W   ");
			else if (i==readIndex)
				System.out.print(" R   ");
			else
				System.out.print("     ");
		}
		System.out.println("\n");
	}
}