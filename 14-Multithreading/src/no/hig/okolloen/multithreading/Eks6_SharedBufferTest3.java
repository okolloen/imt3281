package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Test method to test our home grown multi threading safety net
 */
public class Eks6_SharedBufferTest3 {
	public static void main(String[] args) {
		// Create a pool of threads
		ExecutorService application = Executors.newCachedThreadPool();
		
		// Create a synchronized buffer
		Buffer sharedLocation = new SynchronizedBuffer2();
		
		// Display some heading information in the console
		System.out.printf ("%-40s%s\t\t%s\n%-40s%s\n\n", "Operation", "Buffer", "Occupied", "--------", "-----\t\t----------");
		
		// Start the producer and consumer in separate threads
		application.execute(new Producer(sharedLocation));
		application.execute(new Consumer(sharedLocation));
		
		// Shut down the thread pool once both threads finishes
		application.shutdown();
	}
}
