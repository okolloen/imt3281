package no.hig.okolloen.multithreading;

import java.util.Random;

/*
 * Class to consume numbers from a shared buffer.
 * Ten numbers will be read from a shared buffer, 
 * waiting somewhere between 0 and three seconds
 * between each time a number is read. 
 */
public class Consumer implements Runnable {
	private final static Random generator = new Random();
	// A reference to the shared buffer that we will read from
	private final Buffer sharedLocation;
	
	/*
	 * The constructor takes a reference to the shared buffer as a parameter
	 */
	public Consumer (Buffer shared) {
		// Store the reference to the buffer in this object
		sharedLocation = shared;
	}
	
	/*
	 * This is the code that will be run in the consumer thread
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		// We will keep a running total of the sum of the values read from the buffer
		int sum = 0;
		for (int i=1; i<=10; i++) {
			try {
				// Start of by suspending the thread for somewhere in the range of 0 to three seconds.
				Thread.sleep(generator.nextInt(3000));
				// Fetching a value from the shared buffer, adding it to the sum
				sum += sharedLocation.get();
				// Printing the sum of the currently fetched numbers to the console
				System.out.printf("\t\t\t%2d\n", sum);
			} catch (InterruptedException ie) {
				// If the thread is prematurely awakened
				ie.printStackTrace();
			}
		}
		// We are done fetching the numbers, print out a summary status message. 
		System.out.printf("\n%s %d\n%s\n", "Consumer read values totaling", sum, "Terminating consumer");
	}
}
