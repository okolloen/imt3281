package no.hig.okolloen.multithreading;

import java.util.Random;

/*
 * Class used to produce numbers into a buffer
 * The numbers between 1 and 10 will be produced,
 * with a random delay of between 0 and three second
 * between each number.
 */
public class Producer implements Runnable {
	private final static Random generator = new Random();
	// A reference to the buffer we will produce numbers into
	private final Buffer sharedLocation;
	
	/*
	 * The constructor takes a reference to the shared buffer
	 */
	public Producer (Buffer shared) {
		// Keep the reference to the shared buffer
		sharedLocation = shared;
	}
	
	/*
	 * The run method, this is where we will produce the numbers
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		// Keep a running total of the sum of the values produced
		int sum = 0;
		for (int i=1; i<=10; i++) {
			try {
				// Start of by suspending the thread somewhere between 0 and three secons
				Thread.sleep(generator.nextInt(3000));
				// Set the current number in the shared buffer
				sharedLocation.set (i);
				// Add up the sum
				sum += i;
				// Print out the current sum to the console
				System.out.printf("\t%2d\n", sum);
			} catch (InterruptedException ie) {
				// If the thread awakens prematurely
				ie.printStackTrace();
			}
		}
		// The production of numbers is all done
		System.out.println("Producer done producing\nTerminating producer.");
	}
}
