package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Test class for the Unsynchronized buffer, producer and consumer classes.
 * Creates an UnsynchronizedBuffer, a produced and a consumer and executes the
 * producer and consumer in two separate threads.
 */
public class Eks3_SharedBufferTest {
	public static void main(String[] args) {
		// Creates a new pool of threads
		ExecutorService application = Executors.newCachedThreadPool();
		
		// Creates a new UnsynchronizedBuffer
		Buffer sharedLocation = new UnsynchronizedBuffer();
		
		// Sets up some explanatory text on the console 
		System.out.println("Action\t\tValue\tSum of Produced\tSum of Consumed");
		System.out.println("------\t\t-----\t---------------\t---------------\n");
		
		// Executes the Producer and Consumer in two separate threads
		application.execute(new Producer(sharedLocation));
		application.execute(new Consumer(sharedLocation));
		
		// Shuts down the thread pool when both threads finishes
		application.shutdown();
	}
}
