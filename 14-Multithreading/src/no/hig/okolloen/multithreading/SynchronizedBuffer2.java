package no.hig.okolloen.multithreading;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/*
 * Do it all yourself solution on the synchronization problem
 */
public class SynchronizedBuffer2 implements Buffer {
	// The access lock that we got automatically by using synchronized
	private final Lock accessLock = new ReentrantLock();
	// Two conditions, signaling that we can write or read
	// Connected to the access lock
	private final Condition canWrite = accessLock.newCondition();
	private final Condition canRead = accessLock.newCondition();
	
	// The buffer
	private int buffer = -1;
	// and the flag that signals if the buffer is available or not
	private boolean occupied = false;

	/*
	 * To set the value of the buffer. The buffer will block until space is available.
	 * 
	 * @param value the value to enter into the buffer.
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#set(int)
	 */
	@Override
	public void set(int value) throws InterruptedException {
		// Acquire the lock
		accessLock.lock();
		try {
			while (occupied) { // So long as there is data in the buffer
				System.out.println ("Producer tries to write.");
				displayState ("Buffer full. Producer waits.");
				// Wait for a signal telling us it's OK to proceed 
				canWrite.await();
			}
			// Set the value of the buffer
			buffer = value;
			// The buffer is no longer free for writing
			occupied = true;
			displayState ("Producer writes "+buffer);
			// Send a signal to all threads waiting to read 
			canRead.signalAll();
		} finally {
			// Release the lock
			accessLock.unlock();
		}
	}
	
	/*
	 * To get the value of the buffer. The buffer will block until data is available
	 * 
	 * @return the value of the buffer
	 * 
	 * (non-Javadoc)
	 * @see no.hig.okolloen.multithreading.Buffer#get()
	 */
	@Override
	public int get() throws InterruptedException {
		// Put the value we read into this variable
		int readValue = -1;
		// Acquire the access lock
		accessLock.lock();
		try {
			while (!occupied) {	// loop until data is available
				System.out.println ("Consumer tries to read.");
				displayState ("Buffer empty. Consumer waits.");
				// Wait intil we receive a signal letting us know that data is available
				canRead.await();
			}
			// Data is no longer available
			occupied = false;
			// Get the value of the buffer
			readValue = buffer;
			displayState ("Consumer reads "+buffer);
			// Signal all threads waiting to write to the buffer
			canWrite.signalAll();
		} finally {
			// Release the access lock
			accessLock.unlock();
		}
		// Return the value of the buffer
		return readValue;
	}

	/*
	 * Method to display details about the buffer
	 */
	public void displayState (String operation) {
		System.out.printf ("%-40s%d\t\t%b\n\n", operation, buffer, occupied);
	}
}
