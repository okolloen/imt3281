package no.hig.okolloen.multithreading;

/*
 * This is the old, plain way of doing multithreading.
 */
public class Eks1_SimpleMultithreading {
	public static void main(String[] args) {
		// Encapsulate the object implementing the Runnable in a Thread object
		Thread task1 = new Thread(new PrintTask("Task 1"));
		Thread task2 = new Thread(new PrintTask("Task 2"));
		Thread task3 = new Thread(new PrintTask("Task 3"));

		System.out.println("Threads created.");

		System.out.println("Starting threads...");

		// Starting the threads.
		// NOTE!!!! DO NOT CALL RUN
		// If you do call the run method then it will execute in the current
		// thread
		// That is to say, no multithreading will occur.
		task1.start();
		task2.start();
		task3.start();

		System.out.println("Tasks started, ending main.\n");
	}
}
