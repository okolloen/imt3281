package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Test class for the CircularBuffer.
 */
public class Eks7_CircularBufferTest {
	public static void main(String[] args) {
		// Create a pool of threads
		ExecutorService application = Executors.newCachedThreadPool();
		
		// Create a circular buffer
		CircularBuffer sharedLocation = new CircularBuffer ();
		
		// Show the state of the buffer
		sharedLocation.displayState ("Initial state");
		
		// Start the producer and consumer threads
		// They will show the status of the buffer as
		// things progresses
		application.execute(new Producer(sharedLocation));
		application.execute(new Consumer(sharedLocation));
		
		// Shut down the thread pool once both the threads have completed
		application.shutdown();
	}
}
