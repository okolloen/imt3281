package no.hig.okolloen.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Test class for SynchronizedBuffer
 */
public class Eks5_SharedBufferTest2 {
	public static void main(String[] args) {
		// Create a pool of threads
		ExecutorService application = Executors.newCachedThreadPool();
		
		// Set up a synchronized buffer
		Buffer sharedLocation = new SynchronizedBuffer();
		
		// display some explanatory text in the console
		System.out.printf ("%-40s%s\t\t%s\n%-40s%s\n\n", "Operation", "Buffer", "Occupied", "--------------------", "-------\t\t----------");
		
		// Start the producer and consumer in separate threads
		application.execute(new Producer(sharedLocation));
		application.execute(new Consumer(sharedLocation));
		
		// Shut down the thread pool when both processes finishes
		application.shutdown();
	}
}
