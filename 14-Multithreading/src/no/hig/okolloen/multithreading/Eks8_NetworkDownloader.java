package no.hig.okolloen.multithreading;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

/*
 * Example showing Multithreading used in a GUI application to perform a lengthy process.
 * Here a separate thread is used to download a large image from the Web. 
 */
@SuppressWarnings("serial")
public class Eks8_NetworkDownloader extends JPanel {
	// A progress bar to show the progress of the download operation
	private JProgressBar downloadProgress = new JProgressBar(JProgressBar.HORIZONTAL);
	// A couple of buttons to select the image to download and display
	private JButton img1;
	private JButton img2;
	// GUI component to show the downloaded image.
	private JLabel display = new JLabel ();
	
	/*
	 * The constructor sets up the GUI for the application
	 */
	public Eks8_NetworkDownloader () {
		// Since this is a panel we change the layout to borderlayout
		setLayout (new BorderLayout());
		// The top panel holds the buttons to select the image and the progress bar
		JPanel top = new JPanel ();
		try {	// Need to use an URL wrapper since we fetch the images for the buttons from the web. 
			img1 = new JButton (new ImageIcon (new URL("http://www.free-pictures-photos.com/sunset/sunset-05_small.jpg")));
			img2 = new JButton (new ImageIcon (new URL("http://www.free-pictures-photos.com/sunset/sunbeam-8c3x_small.jpg")));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		// The buttons go in a separate panel with a simple flowlayout
		JPanel buttons = new JPanel ();
		buttons.add(img1);
		// Load image 1
		img1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// The download happens in this method
				download ("sunset-05.jpg");
			}
		});
		buttons.add(img2);
		// Load image 2
		img2.addActionListener(new ActionListener() {
			@Override
			// The download happens in this method
			public void actionPerformed(ActionEvent e) {
				download ("sunbeam-8c3x.jpg");
			}
		});
		// Complete the layout
		top.setLayout(new BorderLayout());
		top.add (buttons, BorderLayout.NORTH);
		top.add (downloadProgress, BorderLayout.CENTER);
		// setIndeterminate says we don't yet know the progress
		// downloadProgress.setIndeterminate(true);
		// Hide the download bar
		downloadProgress.setVisible(false);
		add (top, BorderLayout.NORTH);
		add (new JScrollPane(display), BorderLayout.CENTER);
		setPreferredSize(new Dimension (500, 500));
		setMinimumSize(new Dimension(500, 500));
	}
	
	/*
	 * Method used to start a new thread downloading the selected image
	 * 
	 * @param fname the name of the image to download
	 */
	private void download(String fname) {
		// Create a new thread around a Runnable object
		// Give the full url to download to the Downloader constructor
		Thread down = new Thread (new Downloader ("http://www.free-pictures-photos.com/sunset/"+fname));
		// Start this thread (sure, we could have used an executor :)
		down.start ();
	}

	/*
	 * Class containing the code to be run in a separate thread.
	 * The constructor receives and stores the URL for the file to be downloaded.
	 */
	class Downloader implements Runnable {
		private URL url;
		
		/*
		 * The constructor receives the url (in the form of a string) to be downloaded
		 * 
		 * @para url a String with the address of the file to be downloaded
		 */
		public Downloader (String url) {
			try {
				// Convert the string to a URL
				this.url = new URL (url);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * This is the code that is to run in a separate thread
		 * 
		 * (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		public void run () {
			try {
				// Get the length of the file to download
				int length = url.openConnection().getContentLength();
				// This is the maximum value for the progress bar
				downloadProgress.setMaximum(length);
				// Minimum is of course 0
				downloadProgress.setMinimum(0);
				// Initial value is 0, we haven't downloaded anything yet
				downloadProgress.setValue(0);
				// We now have a download in progress
				// downloadProgress.setIndeterminate(false);
				downloadProgress.setVisible(true);
				// Create a buffer to read into, 8KB is about right
				byte buffer[] = new byte[8*1024];
				// Open the stream from the URL
				InputStream is = url.openStream();
				// Create a temporary file, we know we will be downloading jpg files
				// so give the temporary file that extension
				File f = File.createTempFile("img", ".jpg");
				// Create a fileoutputstream on the temporary file
				FileOutputStream os = new FileOutputStream (f);
				int bytesRead;
				// Read up to 8 KB at a time into the buffer
				while ((bytesRead = is.read(buffer))>-1) {
					// Update the progress bar
					downloadProgress.setValue(downloadProgress.getValue()+bytesRead);
					// Write the data to the local file
					os.write(buffer, 0, bytesRead);
				}
				// Close the output stream
				os.close();
				// Display the newly downloaded image
				display.setIcon(new ImageIcon (f.getAbsolutePath()));
				// And once again we have no download in progress.
				// downloadProgress.setIndeterminate(true);
				downloadProgress.setVisible(false);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	/*
	 * Create a frame and add the panel to it
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame ("Image downloader");
		Eks8_NetworkDownloader nd = new Eks8_NetworkDownloader();
		f.add(nd);
		f.pack();
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
