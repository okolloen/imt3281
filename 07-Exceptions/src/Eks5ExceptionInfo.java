
public class Eks5ExceptionInfo {
	public static void main(String[] args) {
		try {
			method1();
		} catch (Exception e) {
			System.out.printf ("%s\n\n", e.getMessage());
			e.printStackTrace();
			StackTraceElement traceElements[] = e.getStackTrace();
			
			System.out.println("\nStack trace from getStackTrace:");
			System.out.println("Class\t\t\tFile\t\t\tLine\tMethod");
			for (StackTraceElement ste : traceElements) {
				System.out.printf("%s\t", ste.getClassName());
				System.out.printf("%s\t", ste.getFileName());
				System.out.printf("%s\t", ste.getLineNumber());
				System.out.printf("%s\n", ste.getMethodName());
			}
		}
	}
	
	public static void method1() throws Exception {
		method2();
	}

	private static void method2() throws Exception {
		method3();	
	}

	private static void method3() throws Exception {
		throw new Exception ("Exception thrown in method 3");		
	}
}
