
public class Eks4Finally {

	public static void main(String[] args) {
		try {
			throwException();
		} catch (Exception e) {
			System.out.println ("Exception handled in main");
		}
		doesNotThrowException ();
	}

	private static void throwException() throws Exception {
		try {
			System.out.println ("Method throwException");
			throw new Exception ("Exception from throwException");
		} catch (Exception e) {
			System.out.println ("Exception handled in method throwException");
			throw new Exception ("Throw all over to let someone else handle it");
		} finally {
			System.out.println ("Finally executed in throwException");
		}
	}

	private static void doesNotThrowException() {
		try {
			System.out.println("Method doesNotThrowException");
		} catch (Exception e) {
			System.out.println ("Should never be here");
		} finally {
			System.out.println ("Finally executed in doesNotThrowException");
		}
		System.out.println("End of method doesNotThrowException");
	}
}
