import java.util.Scanner;

public class Eks1UtenExceptions {
	public static int quotient (int numerator, int denominator) {
		return numerator/denominator; // kan feile ved dele på 0
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.print("Please enter an integer nominator : ");
		int numerator = scanner.nextInt();
		System.out.print("Please enter an integer denominator : ");
		int denominator = scanner.nextInt();
		int result = quotient(numerator, denominator);
		System.out.printf("\nResult: %d / %d = %d\n", numerator, denominator, result);
		scanner.close();
	}
}
