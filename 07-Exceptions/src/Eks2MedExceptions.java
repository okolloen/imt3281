import java.util.InputMismatchException;
import java.util.Scanner;

public class Eks2MedExceptions {
	public static int quotient (int numerator, int denominator) throws ArithmeticException {
		return numerator/denominator; // kan feile ved dele på 0
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		boolean continueLoop = true;
		do {
			try {
				System.out.print("Please enter an integer nominator : ");
				int numerator = scanner.nextInt();
				System.out.print("Please enter an integer denominator : ");
				int denominator = scanner.nextInt();
				int result = quotient(numerator, denominator);
				System.out.printf("\nResult: %d / %d = %d\n", numerator, denominator, result);
				continueLoop = false;
			} catch (InputMismatchException ime) {
				System.out.printf("\nException : %s\n", ime);
				scanner.nextLine();
				System.out.println ("You must enter integers, please try again\n");
			} catch (ArithmeticException ae) {
				System.out.printf("\nException : %s\n", ae);
				System.out.println ("Zero is an invalid denominator, please try again\n");
			}
		} while (continueLoop);
		scanner.close();
	}
}
