import java.util.Arrays;
import java.util.Random;

public class Eks2ArrayStuff {
	double numbers[] = new double[10000000];
	Random random = new Random();
	
	public Eks2ArrayStuff () {
		System.out.println("Generating ten million numbers");
		for (int i=0; i<5000000; i++)
			numbers[i] = random.nextDouble()*1000;
		numbers[5000000] = 100.0d;
		for (int i=5000001; i<numbers.length; i++)
			numbers[i] = random.nextDouble()*1000;
		System.out.println("Numbers generated with values between 0 and 1000.");
		System.out.println("Sorting the numbers...");
		Arrays.sort(numbers);
		System.out.println("Done sorting");
		System.out.println("Finding the location of the first occurence of 100.0");
		int idx = Arrays.binarySearch(numbers, 100.0d);
		System.out.printf("The number 100.0 was found at location %d\n", idx);
	}
	
	public static void main(String[] args) {
		new Eks2ArrayStuff();
	}

}
