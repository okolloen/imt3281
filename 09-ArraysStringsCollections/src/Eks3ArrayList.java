import java.util.ArrayList;


public class Eks3ArrayList {
	ArrayList<String> tmp = new ArrayList<String>();
	
	private void arrayListOperations () {
		System.out.println("Adding strings to the array list");
		tmp.add("Gjøvik");
		tmp.add("Lillehammer");
		tmp.add("Hamar");
		System.out.println("The list contains " + tmp.size() + " elements");
		boolean b = tmp.contains("Lillehammer");
		System.out.println("The list " + (b ? "contains" : "does not contain")
				+ " the word Lillehammer");
		b = tmp.contains("Oslo");
		System.out.println("The list " + (b ? "contains" : "does not contain")
				+ " the word Oslo");
		System.out.println("The index of the word Lillehammer is "
				+ tmp.indexOf("Lillehammer"));
		System.out.println("Remove Lillehammer from the list");
		tmp.remove("Lillehammer");
		System.out.println("The list contains " + tmp.size() + " elements");
		System.out.println("Removing the element with index 1");
		tmp.remove(1);
		System.out.println("The list contains " + tmp.size() + " elements");
		System.out.println("The element on index 0 is " + tmp.get(0));
		System.out.println("Clearing the list");
		tmp.clear();
		System.out.println("The list contains " + tmp.size() + " elements");
	}
	
	public static void main(String[] args) {
		Eks3ArrayList eks = new Eks3ArrayList();
		eks.arrayListOperations();
	}
	

}
