import java.util.Scanner;


public class Eks6Validate {
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.println("Please enter you first name:");
		String firstName = scanner.nextLine();
		System.out.println("Please enter your last name:");
		String lastName = scanner.nextLine();
		System.out.println("Please enter your address:");
		String address = scanner.nextLine();
		System.out.println("Please enter your city");
		String city = scanner.nextLine();
		System.out.println("Please enter your state:");
		String state = scanner.nextLine();
		System.out.println("Please enter your zip:");
		String zip = scanner.nextLine();
		System.out.println("Please enter your phone:");
		String phone = scanner.nextLine();
		scanner.close();
		
		// Validating
		System.out.println("Validation input....");
		
		if (!validFirstName (firstName))
			System.out.println("Invalid first name");
		else if (!validLastName (lastName))
			System.out.println("Invalid last name");
		else if (!validAddress (address))
			System.out.println("Invalid address");
		else if (!validCity (city))
			System.out.println("Invalid city");
		else if (!validState (state))
			System.out.println ("Invalid state");
		else if (!validZip (zip))
			System.out.println("Invalid zip");
		else if (!validPhone (phone))
			System.out.println("Invalid phone");
		else
			System.out.println("Valid input, good job");
	}

	private static boolean validFirstName(String firstName) {
		// Accept one name only
		return firstName.matches("[A-ZÆØÅ][a-zæøåA-ZÆØÅ]*");
	}

	private static boolean validLastName(String lastName) {
		// Accept one or more names, separated by space, hyphen or '
		return lastName.matches("[A-ZÆØÅ][a-zÆØÅA-ZÆØÅ]+([ '-][A-ZÆØÅa-zÆØÅ]+)*");
	}

	private static boolean validAddress(String address) {
		// One or two names, then whitespace and number
		return address.matches("([A-ZÆØÅa-zæøå]+|[A-ZÆØÅa-zæøå]+\\s[A-ZÆØÅa-zæøå]+)\\s+\\d+");
	}

	private static boolean validCity(String city) {
		// One or two names
		return city.matches("([A-ZÆØÅa-zæøå]+|[A-ZÆØÅa-zæøå]+\\s[A-ZÆØÅa-zæøå]+)");
	}

	private static boolean validState(String state) {
		// One or two names
		return state.matches("([A-ZÆØÅa-zæøå]+|[A-ZÆØÅa-zæøå]+\\s[A-ZÆØÅa-zæøå]+)");
	}

	private static boolean validZip(String zip) {
		// Five digits
		return zip.matches("\\d[5}");
	}

	private static boolean validPhone(String phone) {
		// three-three-four digits, the first two groups must not start with 0
		return phone.matches("[1-9]\\d{2}-[1-9]\\d{2}-\\d{4}");
	}
}
