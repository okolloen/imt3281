import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;


public class Eks8Collections {
	public static void main(String[] args) {
		String cards1[] = { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven" };
		String cards2[] = { "Eight", "Nine", "Ten", "Jack", "Queen", "King" };
		
		// Creating a list
		List<String> face = new ArrayList<String>();
		for (String card : cards1)
			face.add (card);
		
		// Display the contents of the list
		System.out.print("The array list 'face' :");
		for (int i=0; i<face.size(); i++)
			System.out.printf("%s ", face.get(i));
		
		// Creating a linked list
		List<String> face1 = new LinkedList<String>();
		for (String card : cards2)
			face1.add(card);
		
		// Display the contents of the list
		System.out.print("\nThe array list 'face1' :");
		Iterator<String> iterator = face1.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		// Adding face1 to face
		face.addAll(face1);
		System.out.print("\nThe array list 'face' after concatenating the two lists :");
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		// Printing backwards
		System.out.print("\nPrinting the list backwards :");
		ListIterator<String> iterator1 = face.listIterator(face.size());
		while (iterator1.hasPrevious())
			System.out.printf("%s ", iterator1.previous());
		
		// Sorting the list
		System.out.print("\nThe list after sorting :");
		Collections.sort (face);
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		// Sorting the list "my way"
		System.out.print("\nThe list after sorting (now sorting on the substring from the second letter) :");
		Collections.sort (face, new SubstringComparator());
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		// Shuffle the cards
		System.out.print("\nThe list after shuffling the cards: ");
		Collections.shuffle (face);
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		// Searching
		System.out.println("\nSorting the list again to be able to search");
		Collections.sort (face);
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		int idx = Collections.binarySearch(face, "Nine");
		if (idx >=0)
			System.out.printf("\n\n'Nine' found at index : %d", idx);
		else 
			System.out.printf("\n\n'Nine' not found : %d", idx);
		idx = Collections.binarySearch(face, "Elleven");
		if (idx >=0)
			System.out.printf("\n'Elleven' found at index : %d", idx);
		else 
			System.out.printf("\n'Elleven' not found : %d", idx);

		// Stack
		Stack<String> stack = new Stack<String>();
		iterator = face.iterator();
		while (iterator.hasNext())
			stack.push (iterator.next());
		System.out.print("\n\nAll items from the list is pushed on to the stack, now poping them back of :");
		while (!stack.empty())
			System.out.printf("%s ", stack.pop());
		
		// Priority queue
		PriorityQueue<String> pq = new PriorityQueue<String>();
		System.out.print("\nThe list after shuffling the cards: ");
		Collections.shuffle (face);
		iterator = face.iterator();
		while (iterator.hasNext()) {
			String tmp = iterator.next();
			System.out.printf("%s ", tmp);
			pq.offer(tmp);
		}
		System.out.print("\nPicking the items back off the priority queue:");
		while (!pq.isEmpty())
			System.out.printf("%s ", pq.remove());
		
		// Creating doubles
		face.addAll(face1);
		Collections.sort (face);
		System.out.println("\n\nUh-oh");
		Collections.sort (face);
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());		
		// Removing them again
		Set<String> set = new HashSet<String>(face);
		face.clear();
		face.addAll(set);
		System.out.println("\n\nAll duplicates magically gone");
		Collections.sort (face);
		iterator = face.iterator();
		while (iterator.hasNext())
			System.out.printf("%s ", iterator.next());
		
		
		// Finish it of with a map
		String tmp = "This is a sample sentence with several words. This is another sample sentence with several different words.";
		System.out.printf("\n\nCounting words in '%s'\n", tmp);
		Map<String, Integer> map = new HashMap<String, Integer>();
		String tokens[] = tmp.split("[\\s.]");
		// Now count the word occurrences
		for (String token : tokens) {
			if (map.containsKey(token)) {
				int count = map.get(token);
				map.put(token, count+1);
			} else
				map.put(token, 1);
		}
		// Find the words used
		Set<String> keys = map.keySet();
		TreeSet<String> sortedKeys = new TreeSet<String>(keys);
		System.out.println("Word occurences : \nKey\t\tValue");
		for (String key : sortedKeys)
			System.out.printf("%-10s%10s\n", key, map.get(key));
	}
}

class SubstringComparator implements Comparator<String> {
	public int compare (String s1, String s2) {
		return s1.substring(1).compareTo(s2.substring(1));
	}
}