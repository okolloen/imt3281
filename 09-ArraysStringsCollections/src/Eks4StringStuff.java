
public class Eks4StringStuff {
	public static void main(String[] args) {
		String s1 = new String ("hello");
		String s2 = "goodbye";
		String s3 = "Happy Birthday";
		String s4 = "happy birthday";
		
		System.out.printf ("s1 = %s\ns2 = %s\ns3 = %s\ns4 = %s\n\n", s1, s2, s3, s4);
		
		// Test for equality
		if (s1.equals("hello"))
			System.out.println("s1 equals \"hello\"");
		else
			System.out.println("s1 does not equals \"hello\"");
		
		// Test for equality with ==
		if (s1 == "hello")
			System.out.println("s1 is the same object as \"hello\"");
		else
			System.out.println("s1 is NOT the same object as \"hello\"");
		
		// Test for equality, ignore case
		if (s3.equalsIgnoreCase(s4))
			System.out.printf("'%s' equals '%s' with case ignored\n", s3, s4);
		else
			System.out.println("s3 does not equal s4");
		
		// Test compareTo
		System.out.printf("\ns1.compareTo(s2) is %d", s1.compareTo(s2));
		System.out.printf("\ns2.compareTo(s1) is %d", s2.compareTo(s1));
		System.out.printf("\ns1.compareTo(s1) is %d", s1.compareTo(s1));
		System.out.printf("\ns3.compareTo(s4) is %d", s3.compareTo(s4));
		System.out.printf("\ns4.compareTo(s3) is %d\n\n", s4.compareTo(s3));
		
		// Test regionMatches (case sensitive)
		if (s3.regionMatches(0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");

		// Test regionMatches (case sensitive)
		if (s3.regionMatches(true, 0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match with case ignored");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");
		
		// Test method startsWith
		if (s3.startsWith("Happy"))
			System.out.printf("\nThe string '%s' starts with 'Happy'\n", s3);
		else
			System.out.printf("\nThe string '%s' does not start with 'Happy'\n", s3);
		
		// Test method startsWith starting from given position
		if (s4.startsWith("birth", 6))
			System.out.printf("The string '%s' starts with 'birth' at position 6\n", s4);
		else
			System.out.printf("The string '%s' does not start with 'birth' at position 6\n", s4);
		
		// Test method endsWith
		if (s4.endsWith("day"))
			System.out.printf("The string '%s' ends with 'day'\n", s4);
		else
			System.out.printf("The string '%s' does not end with 'day'\n", s4);
		
		// Test indexOf to locate characters in a string
		System.out.printf("\n'a' is found at index %d in \"%s\"\n", s3.indexOf('a'), s3);
		
		// Test lastIndexOf to locate last occurrence in a string
		System.out.printf("Last 'a' is found at index %d in \"%s\"\n", s3.lastIndexOf('a'), s3);
		
		// Test indexOf to locate a substring
		System.out.printf("\"day\" is found at index %d in \"%s\"\n", s3.indexOf("day"), s3);
		System.out.printf("\"hello\" is found at index %d in \"%s\"\n", s3.indexOf("hello"), s3);
		
		// Test substring methods
		System.out.printf("\ns3.substring(6) is : %s\n", s3.substring(6));
		System.out.printf("s3.substring(6,11) is : %s\n", s3.substring(6,11));
		
		// Test concat
		System.out.printf("\ns1.concat(s2) : %s\n", s1.concat(s2));
		
		// Test replace
		System.out.printf("\ns1.replace('l', 'L') : %s\n", s1.replace('l', 'L'));
		
		// Test toUpperCase
		System.out.printf("\ns1.toUpperCase() : %s\n", s1.toUpperCase());
		
		// Test toLowerCase
		System.out.printf("s3.toLowerCase() : %s\n", s3.toLowerCase());
		
		// Test trim
		System.out.printf("\nThe string \"  hei  \\t  \" after trim() : \"%s\"\n", "  hei  \t  ".trim());
	}
}
