
public class Eks5StringBuilder {
	public static void main(String[] args) {
		// Creating a new StringBuilder
		StringBuilder buffer = new StringBuilder ("hello there");
		
		// Getting the contents of a StringBuilder
		System.out.printf("buffer = %s\n", buffer.toString());
		
		// Getting single characters
		System.out.printf("Character at 0: %s\nCharacter at 4: %s\n",	buffer.charAt(0), buffer.charAt(4));
		
		// Setting individual characters
		System.out.println("\nchanging a couple of characters");
		buffer.setCharAt(0, 'H');
		buffer.setCharAt(6, 'T');
		System.out.printf("buffer = %s\n", buffer.toString());
		
		// Current size of StringBuilder
		System.out.printf("\nLength of buffer = %d, capacity of buffer = %d\n",	buffer.length(), buffer.capacity());
		
		// Reverse the buffer
		buffer.reverse();
		System.out.printf("\nLook, backwards : %s\n", buffer.toString());
		buffer.reverse();
		
		// Appending some stuff
		buffer.append("\nThis is another line");
		buffer.append("\n");
		buffer.append(3.1415f);
		System.out.printf ("\nNew contents of buffer : %s\n", buffer.toString());
		
		// Insert some stuff
		buffer.insert(0, "I'm at the start\n");
		buffer.insert(40, "!WOW!");
		System.out.printf ("\nNew contents of buffer : %s\n", buffer.toString());
		
		// Delete some stuff
		buffer.delete(buffer.indexOf("!WOW"), buffer.indexOf("!WOW!")+5);
		buffer.deleteCharAt(1);
		System.out.printf ("\nNew contents of buffer : %s\n", buffer.toString());
		
	}
}
