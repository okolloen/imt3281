package no.hig.imt3281.textPane;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerListModel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/*
 * Example that demonstrates simple usage of styled text in a JTextPane component.
 */
@SuppressWarnings("serial")
public class FormatedText extends JPanel {
	// The JTextPane that we will be using to show the styled text
	JTextPane text = new JTextPane();
	// Need access to the scroll pane containing the text pane
	// This is so that we can control the scrolling of this pane
	JScrollPane textScrollPane;
	JCheckBox bold, italic;
	JSpinner fontSize;
	JTextField newText;
	JComboBox<String> fontName;
	JButton foreColor;
	JButton addText;
	
	public FormatedText () {
		setLayout (new BorderLayout());
		// Add the text pane in a scroll pane
		add (textScrollPane = new JScrollPane (text = new JTextPane()), BorderLayout.CENTER);
		// Add the rest of the layout, to select font name, size, color and bold/italic
		// Also contains controls to add text to the text pane
		add (createLayout(), BorderLayout.SOUTH);
		// A button to select the color for the text that get added to the text pane
		foreColor.addActionListener(e -> {
				// Use a color chooser to select a new text color
				Color c = JColorChooser.showDialog(FormatedText.this, "Velg ny tekstfarge", foreColor.getBackground());
				// If the user has selected a color, set it as the background on the button
				// This is to indicate what color the text will be written in
				if (c!=null)
					foreColor.setBackground(c);
			}
		);
		// The button used to add text to the text pane
		addText.addActionListener(e -> {
				// Call this method to add the text to the text pane
				addText (newText.getText());
				// Clear the textfield
				newText.setText("");
				// and restore the focus to the textfield
				newText.requestFocusInWindow();
			}
		);
		newText.addActionListener (e -> addText.doClick());
	}

	/*
	 * The panel consist of two main parts, the font selection part and the text entry part.
	 * This method joins the two and returns a panel with them combined.
	 */
	private Component createLayout() {
		JPanel layout = new JPanel();
		layout.setLayout (new BorderLayout());
		// This part contains the font selection part (font name, size, color etc)
		layout.add (createFontInfoLayout(), BorderLayout.CENTER);
		// This part contains the text entry part (a textfield and a button
		layout.add (createAddTextLayout(), BorderLayout.SOUTH);
		return layout;
	}

	/*
	 * Create the text entry panel with a titled border.
	 */
	private Component createAddTextLayout() {
		JPanel layout = new JPanel();
		layout.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Legg til tekst"));
		layout.setLayout (new BorderLayout());
		layout.add (newText = new JTextField(), BorderLayout.CENTER);
		layout.add (addText = new JButton ("Legg til"), BorderLayout.EAST);
		return layout;
	}
	
	private void addComponent (Component c, JPanel p, GridBagLayout gbl, GridBagConstraints gbc, int x, int y) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbl.setConstraints(c, gbc);
		p.add (c);
	}
	
	/*
	 * Create the font info panel with a titled border
	 */
	private Component createFontInfoLayout() {
		JPanel fontInfo = new JPanel();
		fontInfo.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Velg utseende"));
		// Using a grid bag layout to achieve a nice layout
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		fontInfo.setLayout(gbl);
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.EAST;
		// Add all the leading labels
		JLabel lFontName = new JLabel ("Velg font: ");
		addComponent(lFontName, fontInfo, gbl, gbc, 1, 1);
		JLabel lFontSize = new JLabel ("Størrlese: ");
		addComponent(lFontSize, fontInfo, gbl, gbc, 1, 2);
		JLabel lBold = new JLabel ("Bold: ");
		addComponent(lBold, fontInfo, gbl, gbc, 1, 3);
		JLabel lItalic = new JLabel("Italic: ");
		addComponent(lItalic, fontInfo, gbl, gbc, 1, 4);
		JLabel lForeground = new JLabel("Foreground: ");
		addComponent(lItalic, fontInfo, gbl, gbc, 1, 5);

		gbc.anchor = GridBagConstraints.WEST;
		
		// Then add the active components
		// First a drop down box to select the font name 
		fontName = new JComboBox<String>(GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames());
		addComponent(fontName, fontInfo, gbl, gbc, 2, 1);

		// Add a spinner to select font size
		Integer fontSizes[] = {8, 10, 12, 14, 16, 18, 24, 32, 64};
		SpinnerListModel fontSizeModel = new SpinnerListModel(fontSizes);
		fontSize = new JSpinner (fontSizeModel);
		fontSize.setValue(12);
		addComponent(fontSize, fontInfo, gbl, gbc, 2, 2);

		// Add a couple of checkboxes, to select bold and italic 
		bold = new JCheckBox();
		addComponent(bold, fontInfo, gbl, gbc, 2, 3);
		italic = new JCheckBox();
		addComponent(italic, fontInfo, gbl, gbc, 2, 4);

		// Then add a button to select the color we want to add text with
		foreColor = new JButton(" ");
		foreColor.setOpaque(true);
		addComponent(foreColor, fontInfo, gbl, gbc, 2, 5);
		foreColor.setBackground(Color.black);
		return fontInfo;
	}

	/*
	 * This method is used to add the text.
	 * 
	 * @param text2 the text to add to the text pane
	 */
	protected void addText(String text2) {
		// A SimpleAttributeSet contains information about the styling to be used when adding new text
		// (or changing selected text.)
		SimpleAttributeSet sas = new SimpleAttributeSet ();
		StyleConstants.setFontFamily (sas, (String) fontName.getSelectedItem());
		StyleConstants.setFontSize (sas, (Integer) fontSize.getValue());
		StyleConstants.setBold(sas, bold.isSelected());
		StyleConstants.setItalic(sas, italic.isSelected());
		StyleConstants.setForeground(sas, foreColor.getBackground());
		// We want to add the text at the end of the existing text
		int pos = text.getStyledDocument().getEndPosition().getOffset();
	    try {
		    	// add the text to the document
		    	text.getStyledDocument().insertString(pos, newText.getText()+"\n", sas);
	    } catch (BadLocationException ble) {
	    		ble.printStackTrace();
	    }
	    // We want to always show the latest text text added, so we need to scroll down
	    // to show all the text.
	    // By putting this operation into the GUI thread we ensure that the previous update
	    // operation has completed before we attempt to scroll down.
	    SwingUtilities.invokeLater (() -> {
	        	// Get the scrollbar from the scroll pane
	        	JScrollBar scrollbar = textScrollPane.getVerticalScrollBar ();
	        	// Set the scrollbar to its maximum value
	        	scrollbar.setValue (scrollbar.getMaximum());
	        }
	    );
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame ("Formatert tekst eksempel");
		f.add(new FormatedText());
		f.setSize(800, 600);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
