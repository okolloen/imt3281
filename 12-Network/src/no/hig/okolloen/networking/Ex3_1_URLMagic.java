package no.hig.okolloen.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Ex3_1_URLMagic {
	public static void main(String[] args) {
		try {
			// URL klassen benyttes for å anngi alle URL type adresser
			URL start = new URL ("http://www.hig.no/");
			// Et URL objekt gjør det enkelt å finne de enkelte bestanddelene av en URL
			System.out.printf ("Protokol: %s\nPort: %s\nPath: %s\nFile: %s\n-----------\n", 
					start.getProtocol(), start.getDefaultPort(), 
					start.getPath(), start.getFile());
			// En URL kan enkelt opprettes med en annen URL som "basis", dvs at relative URL'er 
			// er enkelt å benytte
			URL robots = new URL (start, "robots.txt");
			System.out.printf ("Protokol: %s\nPort: %s\nPath: %s\nFile: %s\n", 
					robots.getProtocol(), robots.getDefaultPort(), 
					robots.getPath(), robots.getFile());
			// URLConnection gir informasjon om selve forbindelsen, det gjør det også
			// mulig å sette informasjon til serveren i forhold til forbindelsen
			URLConnection connection = robots.openConnection();
			System.out.printf ("Content-type: %s\nContent-length: %d\nContent-encoding: %s\n", 
					connection.getContentType(), connection.getContentLength(), 
					connection.getContentEncoding());
			// OpenStream gir tilgang til datastrømmen fra serveren i forbindelse med en URL
			// Vi legger en InputStreamReader utenpå slik at vi får en karakterbassert datastrøm
			// så legger vi en BufferedReader utenpå der igjen for å kunne lese en og en linje
			BufferedReader br = new BufferedReader (new InputStreamReader (robots.openStream()));
			StringBuilder sb = new StringBuilder ();
			String tmp = null;
			// Leser en og en linje fra serveren og bygger opp en streng lokalt
			// Må legge til igjen linjeskift siden readLine fjerner disse
			while ((tmp = br.readLine())!=null) {
				sb.append (tmp);
				sb.append ("\n");
			}
			System.out.printf("\nContent of robots.txt : ------------\n%s", sb.toString());
		} catch (MalformedURLException e) {	// Dersom vi skriver feil i URL'en (feil formatering av en URL)
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {	// Feil på forbindelse til serveren, eller overføring av filen
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}