package no.hig.okolloen.networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

/*
 * Benyttes sammen med SimpleServer, sender en brukerdefinert linje til en server
 * og viser det som taes i mot fra serveren.
 * Serveren antas å befinne seg på localhost og port 3456.
 */
public class Ex2_SimpleClient {
	public static void main(String[] args) {
		// Leser en streng fra brukeren som skal sendes til serveren
		String msg = JOptionPane.showInputDialog("Hva ønsker du å si til serveren?");
		try {
			// Oppretter en forbindelse til localhost på port 3456
			Socket s = new Socket (InetAddress.getLocalHost(), 23456);
			// Oppretter et objekt for å skrive på outputstreamen til forbindelsen
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
			// Oppretter et objekt for å lese fra inputstreamen til forbindelsen
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			// Skriver stringen
			bw.write(msg);
			// Sender et linjeskift
			bw.newLine();
			// Flush sørger for at det som er skrevet faktisk sendes
			bw.flush ();
			String tmp;
			// Benytter en stringbuilder for å bygge opp resultatet, vi vet jo 
			// ikke hvor mange delstrenger det består av.
			StringBuilder response = new StringBuilder();
			// Leser fra leseobjektet inntill vi får null (slutt på fila, dvs serveren har lukket forbindelsen.)
			while ((tmp=br.readLine())!=null) {
				// Legger til linjen lest fra serveren
				response.append (tmp);
				// og legger påigjen linjeskift
				response.append ("\n");
			}
			s.close();
			// Viser beskjeden vi har fått fra serveren.
			JOptionPane.showMessageDialog(null, response.toString());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
