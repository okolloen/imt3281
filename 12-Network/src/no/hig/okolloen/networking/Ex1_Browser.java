package no.hig.okolloen.networking;

import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

/*
 * HyperlinkListener benyttes for å få melding hver gang en bruker trykker på en link
 * i et JEditorPane objekt. Vi kommer tilbake til mer om JEditorPane senere, men det er 
 * en komponent for å vise formatert tekst i et GUI.
 */
@SuppressWarnings("serial")
public class Ex1_Browser extends JFrame implements HyperlinkListener, 
                                               ActionListener {
	
  /*
   * Programmet kan startes med en startside som parameter fra kommandolinja.
   * Dersom ingen parameter er satt så benyttes http://m.hig.no som startside.
   * JEditorPane sliter med moderne, avanserte nettsteder, derfor benytter vi 
   * et enklere nettsted som startside.
   */
  public static void main(String[] args) {
    if (args.length == 0)
      new Ex1_Browser("http://m.hig.no/");
    else
      new Ex1_Browser(args[0]);
  }

  private JButton homeButton;
  private JTextField urlField;
  private JEditorPane htmlPane;
  private String initialURL;

  /*
   * Constructoren setter opp GUI og laster inn startsiden
   */
  public Ex1_Browser(String initialURL) {
    super("Simple Swing Browser");
    this.initialURL = initialURL;
    // addWindowListener(new ExitListener());
    try {
		UIManager.setLookAndFeel(
		        UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

    JPanel topPanel = new JPanel();
    topPanel.setBackground(Color.lightGray);

    homeButton = new JButton(new ImageIcon (getClass().getResource("/images/home.png")));
    homeButton.addActionListener(this);
    // Label som ber brukeren om å taste inn en web adresse
    JLabel urlLabel = new JLabel("URL:");
    // Et tekstfelt for å skrive inn web adressen
    urlField = new JTextField(30);
    urlField.setText(initialURL);
    urlField.setToolTipText("Skriv inn adressen (med http:// først) og trykk enter");
    // Når brukeren trykker enter så skal adressen lastes inn i editor komponenten
    urlField.addActionListener(this);
    topPanel.add(homeButton);
    topPanel.add(urlLabel);
    topPanel.add(urlField);
    // TopPanelet inneholder en hjem knapp og et felt for å skrive inn adressen
    add(topPanel, BorderLayout.NORTH);

    try {
    	// Lager et JEditorPane objekt, laster inn startsiden
        htmlPane = new JEditorPane(initialURL);
        // En HTML side skal ikke redigeres i en nettleser
        htmlPane.setEditable(false);
        // Legger til objektet selv som objektet som skal få beskjed når brukeren trykker på en link
        htmlPane.addHyperlinkListener(this);
        // Legger HTML visningen inn i en scrollpane
        JScrollPane scrollPane = new JScrollPane(htmlPane);
        add(scrollPane, BorderLayout.CENTER);
    } catch(IOException ioe) {
    	// IO feil ved lesing av siden fra URL
       warnUser("Can't build HTML pane for " + initialURL 
                + ": " + ioe);
    }

    // Gjør at vinduet alltid vil dekke en viss del av skjermområdet, uavhengig av skjermstørrelse.
    Dimension screenSize = getToolkit().getScreenSize();
    int width = screenSize.width * 8 / 10;
    int height = screenSize.height * 8 / 10;
    setBounds(width/8, height/8, width, height);
    setVisible(true);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
  }

  /*
   * Metoden kalles når en bruker trykker enter i feltet for å skrive inn en web adresse, eller trykker på hjem knappen
   * Laster inn siden for adressen som er oppgitt i tekstfeltet, eventuelt laster inn hjemmesiden.
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  public void actionPerformed(ActionEvent event) {
    String url;
    if (event.getSource() == urlField)     // Trykket brukerne enter i tekstfeltet 
      url = urlField.getText();
    else  // Clicked "home" button instead of entering URL
      url = initialURL;
    try {
      // Laster inn siden som ønskes vist
      htmlPane.setPage(new URL(url));
      // Oppdaterer "adresselinjen"
      urlField.setText(url);
    } catch(IOException ioe) {
      // Gir en feilmelding dersom det er problemer med å laste siden
      warnUser("Can't follow link to " + url + ": " + ioe);
    }
  }

  /*
   * Når brukeren trykker på en link i "nettleseren" vår så blir denne metoden kallt
   * @see javax.swing.event.HyperlinkListener#hyperlinkUpdate(javax.swing.event.HyperlinkEvent)
   */
  public void hyperlinkUpdate(HyperlinkEvent event) {
	// Dette betyr at vi skal gå til en ny side
    if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
      try {
    	// Vi laster inn den nye siden, event.getURL henter adressen som en URL
        htmlPane.setPage(event.getURL());
        // Oppdaterer "adresselinjen"
        urlField.setText(event.getURL().toExternalForm());
      } catch(IOException ioe) {
        // Gir en feilmelding dersom det er problemer med å laste siden
        warnUser("Can't follow link to " 
                 + event.getURL().toExternalForm() + ": " + ioe);
      }
    }
  }

  /*
   * Viser en feilmelding til brukerne dersom en side ikke kan lastes inn
   */
  private void warnUser(String message) {
    JOptionPane.showMessageDialog(this, message, "Error", 
                                  JOptionPane.ERROR_MESSAGE);
  }
}