package no.hig.okolloen.networking;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

/*
 * A program that can be use as a client for text based Internet services, currently supports ftp, telnet, smtp, http and pop3.
 * Shows messages from the server and allows for commands to be sent to the server. 
 */
@SuppressWarnings("serial")
public class Ex4_Client extends JFrame {
	// The most common text based protocols
	String portNames[] = {"FTP", "Telnet", "SMTP", "HTTP", "POP3"};
	// The corresponding ports
	int ports[] = {21,23, 25, 80, 110};
	// Create a drop down list of the known protocols
	JComboBox<String> selectPort = new JComboBox<String>(portNames);
	// Get the name of the server her
	JTextField serverName = new JTextField (30);
	// Commands to be sent to the server goes here
	JTextField command = new JTextField (30);
	// A button to make it clear that commands can be sent to the server
	JButton sendCommand;
	// A text area used for messages from the server
	JTextArea messages = new JTextArea (20, 20);
	// Reader and writer to communicate with the server
	BufferedReader br = null;
	BufferedWriter bw = null;
	
	public Ex4_Client () {
		super ("Universal Internet Client");
		// The top panel is used to get the name of the server, select the protocol and a connect button
		JPanel top = new JPanel ();
		top.add (new JLabel ("Server name: "));
		top.add (serverName);
		top.add (new JLabel ("  Port: "));
		top.add (selectPort);
		top.add (new JLabel ("   "));
		JButton connect = new JButton ("Koble til");
		top.add (connect);
		// The connect button creates a socket and gets the reader and writer
		connect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// Open a socket to the given server and on the given port
					Socket s = new Socket (InetAddress.getByName (serverName.getText()), ports[selectPort.getSelectedIndex()]);
					// Get the reader and writer, this let us communicate with the server
					br = new BufferedReader (new InputStreamReader (s.getInputStream()));
					bw = new BufferedWriter (new OutputStreamWriter (s.getOutputStream()));
					// Set command input field as the field with the focus
					command.requestFocusInWindow();
				} catch (UnknownHostException uhe) {
					uhe.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		});
		// Add the top line
		add (top, BorderLayout.NORTH);
		// The server status window part goes in the center
		add (new JScrollPane(messages), BorderLayout.CENTER);
		// Create another panel with the entry field for the commands to be sent to the server
		// and the send button
		JPanel south = new JPanel ();
		south.setLayout (new BorderLayout());
		south.add(command, BorderLayout.CENTER);
		// When the user hits enter in the text field we want it to be the same as 
		// pushing the send button
		command.addActionListener (new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendCommand.doClick();
			}
		});
		sendCommand = new JButton ("Send kommando");
		// The send command button
		sendCommand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					// Update the status window with what we're doing
					messages.append ("*** SENDING : "+command.getText());
					messages.append ("\n");
					// Send the command to the server
					bw.write(command.getText());
					// add a newline character
					//  bw.newLine();
					bw.write("\r\n");
					// and we need to flush to make sure it is sent
					bw.flush ();
					// Clear out the command text field and place the focus on the input field again
					command.setText("");
					command.requestFocusInWindow();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		});
		// Adding the bottom line to the layout
		south.add (sendCommand, BorderLayout.EAST);
		add (south, BorderLayout.SOUTH);
		// Pack it and show everything
		pack ();
		setVisible(true);
	}

	/*
	 * This is called from main after the objekt is created i.e. the layout is ready.
	 * Listens to incoming data from the server and shows it in the status area. 
	 */
	private void read () {
		// Looping until the program exits
		while (true) {
			try {
				// Wait here until a connection is established and a reader objekt is initialized
				while (br == null)
					try {
						// More about this next week, but it means suspend this thread for 1/10 second
						Thread.sleep (100);
					} catch (InterruptedException e) {
					}
				String tmp;
				// Reading from the server until eof
				// Will suspend and wait until a line of data is available
				while ((tmp=br.readLine())!=null) {
					// Display the text read from the server in the status area.
					messages.append (tmp);
					messages.append ("\n");
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
	
	/*
	 * Creates an object of the class and starts listening for messages from the server.
	 */
	public static void main(String[] args) {
		Ex4_Client c = new Ex4_Client();
		c.read();
	}
}