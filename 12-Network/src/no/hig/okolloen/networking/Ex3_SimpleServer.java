package no.hig.okolloen.networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * En enkel echo server, tar i mot en linje fra en klient og sender denne tilbake sammen med
 * navnet og adressen til klienten.
 * Lytter på port 3456.
 */
@SuppressWarnings("serial")
public class Ex3_SimpleServer extends JFrame {
	// Tekstområde som brukes for statusmeldinger
	JTextArea status;
	
	public Ex3_SimpleServer () {
		super ("A simple echo server");
		status = new JTextArea ();
		// Legger statusområdet i en scroll pane
		add (new JScrollPane (status));
		// Setter størrelse
		setSize (400, 500);
		// og viser frem vinduet
		setVisible (true);
		// Sørger for at vi avslutter når vinduet lukkes
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// All nettverkshåndtering skjer her
		try {
			// Oppretter en serversocket på port 3456, denne brukes for å ta imot forespørsler
			ServerSocket ss = new ServerSocket (23456);
			Socket s;
			// Godtar en forespørsel om gangen, tilkoblinger buffres i ServerSocket
			// og håndteres "first come, first served"
			while ((s = ss.accept())!=null) {
				// Oppretter en leser på streamen
				BufferedReader br = new BufferedReader (new InputStreamReader (s.getInputStream()));
				// Oppretter et objekt for å skrive til streamen
				BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(s.getOutputStream()));
				// Leser en linje fra klienten
				String tmp = br.readLine();
				// Finner adressen til klienten
				InetAddress remoteAddress = s.getInetAddress();
				// Viser stringen mottatt fra klienten sammen med adressen til klienten
				status.append (String.format ("%s said : %s\n-------------\n", 
						remoteAddress.getHostName(), tmp));
				// Sender en string med adressen (både navn og ip adresse) samt den mottatte stringen
				// tilbake til klienten
				bw.write(String.format ("Hello there visitor from %s (%s)\n'%s' right back at you", 
						remoteAddress.getHostName(), remoteAddress.getHostAddress(), tmp));
				bw.newLine();
				// Viktig å huske å flushe her, hvis ikke vil ikke data bli skrevet riktig.
				bw.flush ();
				// Lukker lese, skrive objektene og selve streamen
				br.close();
				bw.close();
				s.close();
			} // Går i evig løkke her, svarer på stadig nye forespørsler
			ss.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new Ex3_SimpleServer ();
	}
}