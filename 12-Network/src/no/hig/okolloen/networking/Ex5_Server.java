package no.hig.okolloen.networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.io.InputStream;
import java.io.OutputStream;

/*
 * "Full featured" web server, capable of serving html documents and jpg files from the folder website.
 * All header information from the browser is displayed in the server status window for each request.
 * 
 * Non multithreading, responds to one request at a time. Simultanious requests will be buffered in the 
 * serverSocket object.
 */
@SuppressWarnings("serial")
public class Ex5_Server extends JFrame {
	// A textarea for displaying incoming requests.
	private JTextArea status = new JTextArea ();
	// Holds the filename for the resource currently being served.
	private String filename = null;
	// Reference to the serverSocket object
	private ServerSocket ss;
	
	public Ex5_Server () {
		super ("httpd ano 1995");
		// Adds the status area to the window in a ScrollPane
		add (new JScrollPane (status));
		setSize (400, 500);
		// Display the window
		setVisible (true);
		// When closing the window, exit the application
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Now we start listenening to the server, in the main thread.
		// Everything else is handled in the GUI thread.
		// Poor mans multithreading ;)
		try {
			// Opening the server socket, start listening to port 8088
			ss = new ServerSocket (18088);
			Socket s;
			// Accept one request at a time
			while ((s = ss.accept()) != null) {
				// Reads the header sent from the client, initializes the filename
				readHeaderFindFilename (s);
				// Will change the filename to /404.html if file not found
				int length = verifyFileLength();
				
				if (filename.endsWith(".html")) {	// Returns a HTML file
					sendHTMLFile (s, filename, length);
				} if (filename.endsWith(".jpg")) {	// Returns a JPG image
					sendJPGFile (s, filename, length);
				}
				// Close the socket, ready to accept another request
				s.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	/*
	 * Opens a BufferedReader on the socket and reads the request from the client
	 */
	private void readHeaderFindFilename (Socket s) {
		try {
			// Opens a bufferedReader, all requests are text based and ends with a blank line
			BufferedReader br = new BufferedReader (new InputStreamReader (s.getInputStream()));
			String tmp;
			String url = null;
			// Read lines until we come to a blank line
			do {
				tmp = br.readLine ();
				// It's the first line, but this is more generic
				// This line contains the file to fetch
				if (tmp.startsWith("GET ")) {
					url = new String (tmp);
				}
				// Display all lines as they are read
				status.append(tmp);
				status.append ("\n");
			} while (!tmp.equals (""));	// Stop when a blank line is encountered
			// The format for the first line is GET filename HTTP/1.1
			// We want what is between the first and second whitespace
			filename = url.split("[ ]")[1];
			// Translate a single "/" to index.html, that is the normal thing to do
			if (filename.equals("/"))
				filename = "/index.html";
		} catch (IOException ioe) {
			// Hmmm, something fishy, lets try the safest bet
			filename = "/index.html";
		}
	}
	
	/*
	 * Check the length of the file to return. This will also implicitly discover if the requested file
	 * does not exist. If the requested file does not exist we change the filename to 404.html and return that file.
	 * 
	 * @return int the length of the file to send back to the client
	 */
	private int verifyFileLength () {
		try {
			try {
				// We get the length of the file and return it
				 return getClass()
						.getResource("/website" + filename)
						.openConnection().getContentLength();
			} catch (NullPointerException npe) {
				// The file could not be found, we return the 404.html file instead
				filename = "/404.html";
				// Return the length of the 404.html file
				return getClass()
						.getResource("/website" + filename)
						.openConnection().getContentLength();
	
			}
		} catch (IOException ioe) {
			// Should never happen
			return 0;
		}
	}
	
	/*
	 * Send the file with the given filename and length on the output stream of the given socket.
	 * 
	 * @param s the socket from which we will get the output stream
	 * @param filename the name of the file to send back to the client
	 * @param length the number of bytes in the file to be sent back to the client, this is part of the reply header
	 */
	private void sendHTMLFile (Socket s, String filename, int length) {
		try {
			// Open the buffered writer
			BufferedWriter bw = new BufferedWriter (new OutputStreamWriter (s.getOutputStream()));
			// Send everything OK message (I know, we should have sent 404 FILE NOT FOUND for the 404.html file
			bw.write("HTTP/1.0 200 OK");
			// need newline between each line
			// bw.newLine();
			bw.write("\r\n");
			// Content type is needed
			bw.write("Content-type: text/html");
			// bw.newLine();
			bw.write("\r\n");
			// Content length is not mandatory, but real nice
			bw.write("Content-length: "+length);
			// Two newlines to indicate end of header, the rest is the content of the reply
			// bw.newLine();
			bw.write("\r\n");
			// bw.newLine();
			bw.write("\r\n");
			// Opening a bufferedReader on the stream for the requested file
			BufferedReader filereader = new BufferedReader (new InputStreamReader (getClass().getResourceAsStream("/website" + filename)));
			String ftmp;
			// Reading a line, then sending it to the server
			while ((ftmp = filereader.readLine())!=null) {
				bw.write(ftmp);
				// bw.newLine();
				bw.write("\r\n");
			}
			// ALWAYS remember to flush (your mother told you so to ;)
			bw.flush();
			bw.close();
		} catch (IOException ioe) {
			
		}
	}

	/*
	 * Sending an image file back to the browser.
	 * 
	 * @param s the socket for the connection, used to get the output stream
	 * @param filename need to find the file to send back to the client
	 * @param length the number of bytes to send back to the client
	 */
	private void sendJPGFile (Socket s, String filename, int length) {
		try {
			// Get the output stream first, we need this for transfering the image data later on
			OutputStream os = s.getOutputStream();
			// Wrap a buffered writer around the output stream
			BufferedWriter bw = new BufferedWriter (new OutputStreamWriter (os));
			// Sending the header stuff
			bw.write("HTTP/1.0 200 OK");
			// bw.newLine();
			bw.write("\r\n");
			// This is a image, of type jpg
			bw.write("Content-type: image/jpg");
			// bw.newLine();
			bw.write("\r\n");
			bw.write("Content-length: "+length);
			// a blank line indicates the end of the header information
			// bw.newLine();
			bw.write("\r\n");
			// bw.newLine();
			bw.write("\r\n");
			// Need to flush, because we will now start sending binary data directly on the stream
			bw.flush();
			// The the input stream for the file to send
			InputStream is = getClass().getResourceAsStream("/website" + filename);
			// A buffer used to read/write the file
			byte buffer[] = new byte[1024];
			int bytesRead;
			// Read up to 1024 bytes from the file. A return value of -1 indicates end of file
			while ((bytesRead = is.read (buffer))>-1) {
				// Write the corresponding bytes to the output stream
				os.write(buffer, 0, bytesRead);
			}
			// Again, we flush and close
			os.flush();
			os.close();
		} catch (IOException ioe) {
			
		}
	}

	// Start the server prosess.
	public static void main(String[] args) {
		new Ex5_Server();
	}
}
