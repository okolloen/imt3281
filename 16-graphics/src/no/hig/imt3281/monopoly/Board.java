package no.hig.imt3281.monopoly;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.io.ObjectInputStream.GetField;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Board extends JPanel {
	private Image board;
	private Dimension boardSize; 
	private ImageIcon pieces[] = new ImageIcon[3];
	
	public Board () {
		ImageIcon tmpBoard = new ImageIcon(getClass().getResource("/images/Monopoly_Aberdeen_Edition_board.jpg"));
		boardSize = new Dimension (tmpBoard.getIconWidth(), tmpBoard.getIconHeight());
		board = tmpBoard.getImage();
		pieces[0] = new ImageIcon(getClass().getResource("/images/fingerbol.png"));
		pieces[1] = new ImageIcon(getClass().getResource("/images/flosshatt.png"));
		pieces[2] = new ImageIcon(getClass().getResource("/images/trillebar.png"));
	}
	
	public void paint (Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(board, 0, 0, null);
		AffineTransform transform = new AffineTransform();
		transform.translate(-pieces[0].getIconWidth()/4, -pieces[0].getIconHeight()/4);
		transform.translate(110, 280);
		transform.scale(.5, .5);
		transform.rotate(Math.PI/2);
		g2d.drawImage(pieces[0].getImage(), transform, null);
		transform = new AffineTransform();
		transform.translate(-pieces[1].getIconWidth()/4, -pieces[1].getIconHeight()/4);
		transform.translate(60, 420);
		transform.scale(.5, .5);
		g2d.drawImage(pieces[1].getImage(), transform, null);
		g2d.drawImage(pieces[2].getImage(), 20, 550, null);
		g2d.setStroke(new BasicStroke(4));
		g2d.setPaint(new Color (.9f, .1f, .1f));
		g2d.fillOval(40, 900, 50, 50);
		g2d.setPaint(Color.RED);
		g2d.drawOval(40, 900, 50, 50);
	}
	
	public Dimension getPreferredSize() {
		return new Dimension (boardSize);
	}
	
	public Dimension getMinimumSize() {
		return new Dimension (boardSize);
	}
}
