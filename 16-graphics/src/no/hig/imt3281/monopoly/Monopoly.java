package no.hig.imt3281.monopoly;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

public class Monopoly extends JFrame {
	
	public Monopoly () {
		add (new JScrollPane(new Board()));
		pack ();
		setVisible (true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new Monopoly();
	}
}
