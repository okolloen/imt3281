package no.hig.okolloen.logging;

import java.util.logging.Logger;

/**
 * Set up and use a single logger
 * 
 * @author IMT3281
 *
 */
public class Eks1 {
	// Get the logger for Eks1
	private static Logger logg = Logger.getLogger(Eks1.class.getPackage().toString());
	
	/**
	 * Get the Logger object
	 * @return the logger object to use for logging
	 */
	public static Logger getLogger () {
		return logg;
	}

	public static void main (String args[]) {
		// main har tilgang til logg siden logg er statisk i klassen
		logg.info("Main har startet");
		try {
			throw new Exception ("Ny exception er kastet i main");
		} catch (Exception e) {
			logg.warning("En feil ble håndtert i main");
		}
		logg.severe("Avslutter programmet");
	}
}
