package no.hig.okolloen.logging;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Program used as a server for programs sending log output to the network.
 * NOTE, this only handles one running program at a time! This server listens
 * to port 3333.
 * 
 * @author IMT3281
 *
 */
public class NetworkLogServer extends JFrame {
	JTextArea output = new JTextArea();
	
	/**
	 * Constructor, sets up a textarea to show incoming messages and starts listening to port 3333.
	 * Single threaded, so will only handle one client at a time.
	 */
	public NetworkLogServer () {
		super ("Logging server");
		// Standard GUI setup
		add (new JScrollPane(output));
		setSize (500, 400);
		setVisible (true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		try {	// Creates a serversocket and listens for incoming calls
			ServerSocket ss = new ServerSocket (3333);
			Socket s;
			while ((s=ss.accept())!=null) {	// Once a client connects, read from that client
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				String tmp = null;
				while ((tmp = br.readLine())!=null)	// keep reading until eof (that is, the client program terminates.
					output.append(tmp+"\n");
			}
		} catch (Exception e) {	// on error, log to same logger as this program is a server for. (Is this a good idea?????)
			MyLogging.getLogger().warning("Feil oppstått i log serveren");
		}
	}
	
	public static void main(String[] args) {
		new NetworkLogServer();
	}

}
