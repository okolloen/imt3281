package no.hig.okolloen.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.XMLFormatter;

/**
 * Show logging to file in XML format, still use the logger object from eks1.
 * 
 * @author IMT3281
 *
 */
public class Eks3 {
	public static void main(String[] args) {
		// Get logger
		Logger logg = Eks1.getLogger();
		// Log most everything
		logg.setLevel(Level.FINE);
		logg.setUseParentHandlers(false);		// Stops console log
		// Set up a new file handler
		FileHandler fh = null;
		try {
			fh = new FileHandler("./loggTilFil.log");
			logg.fine("Handler -> MemoryHandler, FileHandler, ConsoleHandler, SocketHandler");
			fh.setLevel(Level.INFO);
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Set up an XML formatter
		XMLFormatter xml = new XMLFormatter ();
		logg.fine ("Formatter -> SimpleFormatter, XMLFormatter");
		// Set the formatter for the log handler
		fh.setFormatter(xml);
		// add the log handler to the logger
		logg.addHandler(fh);
		// Log some events
		logg.severe("Dette er alvorlig");
		logg.warning("Advarsel");
		logg.info("Til informasjon");
		logg.fine("Bagateller");
		logg.finest("Knapt verdt å nevne");
	}
}
