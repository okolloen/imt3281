package no.hig.okolloen.logging;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bruk logger objektet fra forrige eksempel
 * 
 * @author IMT3281
 *
 */
public class Eks2 {
	public static void main(String[] args) {
		// getLogger er en statisk metode i Eks1.
		Logger logg = Eks1.getLogger();
		// Endre hvilke meldinger som skal logges
		logg.setLevel(Level.WARNING);
		// Logg en del hendelser av ulik karakter
		logg.severe("Dette er alvorlig");
		logg.warning("Advarsel");
		logg.info("Til informasjon");
		logg.fine("Bagateller");
		logg.finest("Knapt verdt å nevne");
	}

}
