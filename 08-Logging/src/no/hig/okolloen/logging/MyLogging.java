
package no.hig.okolloen.logging;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Class to get a Logger object, initialized with the properties from config/logging.properties.
 * Designed with the factory pattern/singleton pattern.
 * 
 * @author IMT3281
 *
 */
public class MyLogging {
	private static Logger log=null;
	private static MyLogging o = new MyLogging ();
	
	/**
	 * Method used to access the Logger object.
	 * The first call to this method will create a new logger and initialize it 
	 * with the properties from the file config/logging.properties. Subsequent calls
	 * will return a reference to this same object.
	 * 
	 * @return a Logger object to be used to log events
	 */
	public static Logger getLogger () {
		if (log!=null)	// If we already have created a logger, return it
			return log;
		// No logger exists so create a new one
		log = Logger.getLogger("PrimaryLogger");
		// Log everything, this will be reduced once the config file is read
		log.setLevel(Level.ALL);
		// Note that this will output this message to standard console
		log.info("initializing - trying to load configuration file ...");
		try {
			// Get a handle to config/logging.properties
			InputStream is = o.getClass().getResourceAsStream("/config/logging.properties");
			// Read and parse the configuration
		    LogManager.getLogManager().readConfiguration(is);
		} catch (IOException ex) {
			// Something went wrong configuring the logging system, return to default
			log.setLevel(Level.INFO);
		    System.out.println("WARNING: Could not open configuration file");
		    System.out.println("WARNING: Logging not configured (console output only)");
		}
		log.info("Logging initialized");
		// Return the newly created logger
		return log;
	}

	/**
	 * A simple test of the log handling
	 * 
	 * @param srgs
	 */
	public static void main (String srgs[]) {
		// Get a logger
		Logger log = getLogger();
		// Log some events of different kinds
		log.severe("Alvorlig");
		log.warning("Advarsel");
		log.info("Til informasjon");
		log.fine("Ikke så viktig");
		log.finest("Knapt verdt å nevne");
	}

}
