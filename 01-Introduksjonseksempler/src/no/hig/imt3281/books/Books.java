package no.hig.imt3281.books;

public class Books {
	Book books[] = new Book[10];

	public Books () {
		books[0] = new Book ("Java how to program", "Deitel & Deitel", 750);
		books[1] = new Book ("Java for programmers", "Deitel & Deitel", 800);
		
		System.out.println(books[0]);
		System.out.println("=================");
		System.out.println(this);
	}
	
	public String toString () {
		String s = "";
		float sum = 0;
		for (int i=0; i<books.length; i++) {
			if (books[i]!=null) {
				s += books[i]+"\n";
				sum += books[i].getPrice();
			}
		}
		return s+"\tTotal cost of the books is: "+sum;
	}
	
	public static void main (String args[]) {
		new Books();
	}
}
