
import javax.swing.*;

public class HelloYou {

	public static void main(String[] args) {
		String name = JOptionPane.showInputDialog("Hva heter du?");
		if (name!=null) {
			JOptionPane.showMessageDialog(null, "Velkommen til Java "+name);
		} else {
			JOptionPane.showMessageDialog(null, "Neivelda");
		}
	}
}
