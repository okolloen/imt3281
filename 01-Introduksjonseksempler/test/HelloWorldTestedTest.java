import static org.junit.Assert.*;

import org.junit.Test;


public class HelloWorldTestedTest {

	@Test
	public void testToString() {
		HelloWorldTested helloWorldTested = new HelloWorldTested ();
		assertEquals ("Hello World", helloWorldTested.toString());
	}

}
