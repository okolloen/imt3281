import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Ex2Menu extends JFrame {
	private final Color colorValues[] = { Color.BLACK, Color.BLUE, Color.RED, Color.GREEN };
	private JRadioButtonMenuItem colorItems[];
	private JRadioButtonMenuItem fonts[];
	private JCheckBoxMenuItem styleItems[];
	private JLabel displayLabel;
	private ButtonGroup fontButtonGroup, colorButtonGroup;
	private int style = Font.PLAIN;
	
	public Ex2Menu () {
		super ("Menu testing");
		
		// Create the file menu
		JMenu fileMenu = new JMenu ("File");
		fileMenu.setMnemonic ('F');
		
		// Create the about menu item
		JMenuItem aboutItem = new JMenuItem ("About...");
		aboutItem.setMnemonic ('A');
		fileMenu.add (aboutItem);
		// Handle the action from the about menu item
		aboutItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				JOptionPane.showMessageDialog (Ex2Menu.this, "Et eksempel på\nbruk av menyer", "About", JOptionPane.PLAIN_MESSAGE);
			}
		});
		









		// Create the exit menu item
		JMenuItem exitItem = new JMenuItem ("Exit");
		exitItem.setMnemonic ('X');
		// Add a separator in the file menu
		fileMenu.addSeparator ();
		fileMenu.add (exitItem);
		// Handle the Exit menu item
		exitItem.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				System.exit (0);
			}
		});










		
		// Create the actual menu bar
		JMenuBar bar = new JMenuBar ();
		// Setting it as the frames menu bar
		setJMenuBar (bar);
		// Adding the file menu to the menu bar
		bar.add (fileMenu);













		
		// Create the format menu
		JMenu formatMenu = new JMenu ("Format");
		formatMenu.setMnemonic ('F');
		
		// Create the color selection menu
		String colors[] = { "Black", "Blue", "Red", "Green" };
		JMenu colorMenu = new JMenu ("Color");
		colorMenu.setMnemonic ('C');
		
		// Create color selection menu items array
		colorItems = new JRadioButtonMenuItem[colors.length];
		colorButtonGroup = new ButtonGroup ();
		ItemHandler itemHandler = new ItemHandler ();
		
		// Creating and setting up individual color selection menu items
		for (int i=0; i<colors.length; i++) {
			colorItems[i] = new JRadioButtonMenuItem (colors[i]);
			colorMenu.add (colorItems[i]);
			colorButtonGroup.add (colorItems[i]);
			colorItems[i].addActionListener (itemHandler);
		}
		colorItems[0].setSelected (true);
		formatMenu.add (colorMenu);
		formatMenu.addSeparator ();
		












		// Creating the font selection menu
		String fontNames[] = { "Serif", "Monospaced", "SansSerif" };
		JMenu fontMenu = new JMenu ("Font");
		fontMenu.setMnemonic ('F');
		fonts = new JRadioButtonMenuItem[fontNames.length];
		fontButtonGroup = new ButtonGroup ();
		for (int i=0; i<fontNames.length; i++) {
			fonts[i] = new JRadioButtonMenuItem(fontNames[i]);
			fontMenu.add (fonts[i]);
			fontButtonGroup.add (fonts[i]);
			fonts[i].addActionListener (itemHandler);
		}
		fonts[0].setSelected(true);
		fontMenu.addSeparator ();
		












		// Add the bold/italic part of the font selection menu
		String styleNames[] = { "Bold", "Italic" };
		styleItems = new JCheckBoxMenuItem[styleNames.length];
		StyleHandler styleHandler = new StyleHandler ();
		for (int i=0; i<styleNames.length; i++) {
			styleItems[i] = new JCheckBoxMenuItem (styleNames[i]);
			fontMenu.add (styleItems[i]);
			styleItems[i].addItemListener (styleHandler);
		}
		
		// Add the font menu to the format menu, and then add the format menu 
		// to the menu bar
		formatMenu.add (fontMenu);
		bar.add (formatMenu);
		









		// Set up the label to display some demo text
		displayLabel = new JLabel ("Some sample text", SwingConstants.CENTER);
		displayLabel.setForeground (colorValues[0]);
		displayLabel.setFont (new Font ("Serif", Font.PLAIN, 72));
		
		add (displayLabel, BorderLayout.CENTER);
		
		bar.add (formatMenu);
	}
	









	class ItemHandler implements ActionListener {
		public void actionPerformed (ActionEvent ae) {
			// Handle font color selection
			for (int i=0; i<colorItems.length; i++) {
				if (colorItems[i].isSelected()) {
					displayLabel.setForeground (colorValues[i]);
					break;
				}
			}
			
			// Handle font name selection
			for (int i=0; i<fonts.length; i++) {
				if (ae.getSource() == fonts[i]) {
					displayLabel.setFont (new Font (fonts[i].getText(), style, 72));
				}
			}
			
			// Tegn opp igjen applikasjonen
			repaint ();
		}
	}
	











	// Handle bold/italic
	class StyleHandler implements ItemListener {
		public void itemStateChanged (ItemEvent ae) {
			// Fetch name of currently used font
			String name = displayLabel.getFont().getName();
			
			// Build up the font style
			style = Font.PLAIN;
			style += styleItems[0].isSelected()?Font.BOLD:0;
			style += styleItems[1].isSelected()?Font.ITALIC:0;
			
			displayLabel.setFont (new Font (name, style, 72));
		}
	}
	





	public static void main (String args[]) {
		Ex2Menu ex = new Ex2Menu ();
		ex.setSize (700, 300);
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}














