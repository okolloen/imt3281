import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

@SuppressWarnings("serial")
public class Ex1Sliders extends JFrame {
	// OvalPanel is my own class to draw an oval
	private OvalPanel drawingPanel;
	// JSlider is the class containing the sliders
	private JSlider ovalHeight, ovalWidth;
	
	public Ex1Sliders () {
		super ("Slider demo");
		// Creating a panel with an oval
		drawingPanel = new OvalPanel();
		drawingPanel.setBackground (Color.YELLOW);
		// Creating the vertical slider to controll the height of the oval
		ovalHeight = new JSlider (SwingConstants.VERTICAL, 0, 200, 10);
		ovalHeight.setMajorTickSpacing (10);
		ovalHeight.setPaintTicks (true);
		// Creating the horizontal slider to controll the width of the oval
		ovalWidth = new JSlider (SwingConstants.HORIZONTAL, 0, 200, 10);
		ovalWidth.setMajorTickSpacing (10);
		ovalWidth.setPaintTicks (true);
		
		// The meassurementListener listens to changes in the sliders
		MeassurementListener ml = new MeassurementListener ();
		ovalHeight.addChangeListener (ml);
		ovalWidth.addChangeListener (ml);
		
		// Adding all the components to the layout
		add (drawingPanel, BorderLayout.CENTER);
		add (ovalHeight, BorderLayout.WEST);
		add (ovalWidth, BorderLayout.SOUTH);
	}
	
	// An object of this class has its stateChanged method called every time
	// a slider changes its value
	class MeassurementListener implements ChangeListener {
		public void stateChanged (ChangeEvent ce) {
			// Set the new size of the oval to draw
			drawingPanel.setSize (ovalWidth.getValue(), ovalHeight.getValue());
			// Redraw that panel
			drawingPanel.repaint ();
		}
	}
	
	// Panel containing an oval
	class OvalPanel extends JPanel {
		// Starting with and height of the oval
		private int height = 10, width = 10;
		
		// The oval is drawn here
		public void paintComponent (Graphics g) {
			// First we make it a nice empty panel
			super.paintComponent (g);
			// Then we draw the oval, from the bottom left corner
			g.fillOval (0, 200-height, width, height);
		}
		
		// Whooops, setSize really does mean something else
		// but here it sets the size of the oval
		public void setSize (int width, int height) {
			this.height = (height>=0?height:0);
			this.width = (width>=0?width:0);
		}
		
		// Override this method to create room for this panel to draw ovals
		// up to 200x200 in size
		public Dimension getPreferredSize () {
			return new Dimension (200, 200);
		}
		
		// Just confirming that we really need this size
		public Dimension getMinimumSize () {
			return getPreferredSize ();
		}
	}
		
	public static void main (String args[]) {
		Ex1Sliders ex = new Ex1Sliders ();
		ex.pack ();
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}