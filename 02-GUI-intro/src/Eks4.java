import javax.swing.*;

/**
 * Shows a window with a button i a panel
 * 
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks4 extends JFrame {

	/**
	 * Default constructor that sets up the GUI.
	 */
	public Eks4 () {
		super ("Hello world");					// Set the title
		JButton b = new JButton ("Press here");	// Create the button
		JPanel p = new JPanel ();				// Create a new panel (blank piece of paper)
		p.add (b);								// Add the button to the panel
		add (p);								// Add the panel to the frame
		pack();									// Make just enough room
		setVisible (true);						// Show the window
	}
	
	public static void main (String args[]) {
		new Eks4 ();
	}
}