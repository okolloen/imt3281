# README #

This repo contains source code for the subject IMT3281 at Gjøvik University College.

### What is this repository for? ###

* Whenever errors are corrected or updates are made this repo will be updated. By pulling down the latest version of this repo you will ensure that you always have the latest version of the code.

### How do I get set up? ###

* Clone this repo into a directory of you choosing and then start up eclipse with that directory/imt3281 as its workspace. Then do File->Import->Existing projects into workspace. Select the directory and import all projects. You should now have separate projects for all the lectures which makes finding the code you are looking for easy.
* It is assumed that you are using Eclipse. If not, you are on your own. 
* You should have Eclipse with Java 8 support.
