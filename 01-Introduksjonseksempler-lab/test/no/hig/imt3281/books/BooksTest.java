package no.hig.imt3281.books;

import static org.junit.Assert.*;

import java.util.ResourceBundle;

import org.junit.Test;

/**
 * Tests for the Books class.
 * 
 * @author okolloen
 *
 */
public class BooksTest {

	@Test
	public void testBooks() {	// Checks that the messages reference in Books is correct
		Books.messages = ResourceBundle.getBundle("i18n.introduction");
		assertTrue(Books.messages instanceof ResourceBundle);
	}
	
	@Test
	public void testToString() {	// Checks the toString method (without any books)
		Books b = new Books ();
		Books.messages = ResourceBundle.getBundle("i18n.introduction");
		String goal = "\t"+Books.messages.getString("totalCost")+": 0.0";
		assertEquals(goal, b.toString());
	}

	@Test
	public void testToHTML() {		// Checks the toHTML method (without any books)
		Books b = new Books ();
		Books.messages = ResourceBundle.getBundle("i18n.introduction");
		String goal = "<html><body><table><tr><td><b>"+Books.messages.getString("title")+
				"</b></td><td><b>"+Books.messages.getString("author")+
				"</b></td><td><b>"+Books.messages.getString("price")+"</b></td></tr>\n"+
				"<tr><td colspan='2' align='right'>"+Books.messages.getString("totalCost")+
				"</td><td>0.0</td></tr></table></body></html>";
		assertEquals(goal, b.toHTML());
	}
}
