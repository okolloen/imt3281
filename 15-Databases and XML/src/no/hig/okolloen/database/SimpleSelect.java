package no.hig.okolloen.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Example showing how to connect to a database and perform a simple select operation
 */
public class SimpleSelect {
	static final String DATABASE_URL = "jdbc:mysql://stan.hig.no/imt3281-eks";
	
	public static void main(String[] args) {
		// Used to hold the connection to the database
		Connection con = null;
		// Used to execute statements on the database (SELECT, INSERT, UPDATE, DELETE etc)
		Statement stmt = null;
		// Used to hold the result of a database SELECT operation
		ResultSet res = null;
		
		try {
			// Connect to the database, the database URL contains all the information needed
			// and then the username and password is passed along as well
			con = DriverManager.getConnection(DATABASE_URL, "imt3281-eks", "imt3281");
			// Create the statement object on the connection
			stmt = con.createStatement();
			// Execute a query and return the result
			res = stmt.executeQuery("SELECT givenname, surename, email from users where employee='y' ORDER BY surename");
			
			// The meta data contains information about the result
			ResultSetMetaData meta = res.getMetaData();
			// such as the number of column returned
			int numCols = meta.getColumnCount();
			System.out.println("Ansatte ved HiG");
			for (int i=0; i<numCols; i++)
				// The meta data also contains the names of the columns
				// NOTE!!!!! the index starts at 1 (ONE)
				System.out.printf("%-20s\t", meta.getColumnName(i+1));	// Indeksen starter på 1 (EN!!!)
			System.out.println();
			// The result set contains a pointer that is placed in front of the first row
			// we loop through the result as long as that pointer can be moved ahead
			while (res.next()) {
				// the while moves us through the rows of the result, now go through the columns
				for (int i=0; i<numCols; i++)
					// Write out every column 
					System.out.printf("%-20s\t", res.getObject(i+1));		// Indeksen starter på 1 (EN!!!)
				System.out.println();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				// In the end we need to close the resultset, the statement and the connection
				// Nothing bad happens if we forget it here since we only do a simple select
				// but it's good practice anyway
				res.close();
				stmt.close();
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}