package no.hig.okolloen.database.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * @see CreateDatabase for the code to create the database. Here we create a table in the newly created database.
 */
public class CreateTable {
	// Note that the ;create=true has gone.
	private final static String url = "jdbc:sqlite:MyDbTest.db";
	
	public static void main(String[] args) {
		try {
			Class.forName("org.sqlite.JDBC");
			// This is just as any other database
			Connection con =  DriverManager.getConnection(url);
			// Create a statement
			Statement stmt = con.createStatement();
			// use execute to perform CREATE, INSERT, UPDATE, DELETE, executeQuery is for SELECT
			// MySQL has auto_increment, Derby har GENERATED values
			stmt.execute("CREATE TABLE contacts (id INTEGER PRIMARY KEY AUTOINCREMENT, "+
					"givenname varchar(128) NOT NULL, "+
					"surename varchar(128) NOT NULL, "+
					"email varchar(128) NOT NULL, "+
					"phone varchar(32))");
			// And close
			con.close();
			System.out.println("Table 'contacts' created");
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
