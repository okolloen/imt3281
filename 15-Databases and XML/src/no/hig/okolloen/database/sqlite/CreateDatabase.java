package no.hig.okolloen.database.sqlite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * Being able to have access to a relational database without all the fuss of a database server is nice.
 * The Java DB/Apache Derby is just that. Download from : http://db.apache.org/derby/derby_downloads.html
 * This code uses the Apache Derby embeded database system to create a new database.
 */
public class CreateDatabase {
	// Connect using jdbc, and the derby subprotocol, connect to database MyDbTest
	// and, create the database as well (scary stuff, can overwrite existing DB
	private final static String url = "jdbc:sqlite:MyDbTest.db";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// Connect, and create
			Class.forName("org.sqlite.JDBC");
			System.out.println("Connecting to database for the first time, database is created");
			Connection con =  DriverManager.getConnection(url);
			// Then just close the connection, the database is now created
			con.close();
			System.out.println ("Database is created.");
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
