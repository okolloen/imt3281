package no.hig.okolloen.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.AbstractTableModel;

/*
 * A table model using a database table to hold the data
 */
@SuppressWarnings("serial")
public class ResultSetTableModel extends AbstractTableModel {
	// The connection to the database
	private Connection con;
	// The statement object used to perform the select stamement
	private Statement stmt;
	// The resultset contains the data from the database
	private ResultSet res;
	// The meta data gives us the number of columns and the name of the columns
	private ResultSetMetaData meta;
	// We can find the number of rows
	private int numRows;
	// If we are not connected to the database there is nothing much we can do
	private boolean connectedToDatabase = false;
	
	/*
	 * The constructor gets the database url, usernaem and password and the initial sql query.
	 * Connects to the database, creates the statement object and make sure the initial query is performed.
	 * Passes any SQL errors straight on the caller
	 */
	public ResultSetTableModel (String url, String username, String password, String query) throws SQLException{
		// Connect to the database
		con = DriverManager.getConnection(url, username, password);
		// Create a statement, normally a result set can only be traversed from beginning to end
		// but we indicate that this result set should be made in such a way that we can scroll both ways
		stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		// We are connected to the database
		connectedToDatabase = true;
		// Perform the sql query to fill this table model with data
		setQuery (query);
	}
	
	/*
	 * Get the class for the given column.
	 * Use the meta data objects getColumnClassName to fetch the class name
	 * for the column and then return the class for that name.
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) throws IllegalStateException {
		if (!connectedToDatabase)	// If we are not connected throw an exception 
			throw new IllegalStateException("Not connected to Database");
		try {
			// Find the class name
			String className = meta.getColumnClassName(columnIndex+1);
			// return the class for that name
			return Class.forName(className);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Whooops, just return Object, that never fails
		return Object.class;
	}
	
	/*
	 * The column count is easily available in the meta data
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() throws IllegalStateException {
		if (!connectedToDatabase) // If not connected throw an exception
			throw new IllegalStateException("Not connected to Database");
		try {
			// Return the number of columns
			return meta.getColumnCount();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		// Should never be here
		return 0;
	}

	/*
	 * The column names is also easily available from the meta data
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName(int column) throws IllegalStateException {
		if (!connectedToDatabase)	// If not connected, throw an exception 
			throw new IllegalStateException("Not connected to Database");
		try {
			// Return the name of the column
			return meta.getColumnName(column+1);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		// Well, again, we should never end up here
		return "";
	}
	
	/*
	 * We find the number of rows when we perform the sql query, just return that number
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() throws IllegalStateException {
		if (!connectedToDatabase)	// If not connected, throw an exception 
			throw new IllegalStateException("Not connected to Database");
		// Return the number of rows in the result
		return numRows;
	}

	/*
	 * Get the value at the given row and column from the result set
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int col) throws IllegalStateException {
		if (!connectedToDatabase)	// If not connected, throw an exception 
			throw new IllegalStateException("Not connected to Database");
		try {
			// Move to the right row (Again, NOTE, the index starts at 1 (ONE)
			res.absolute(row+1);
			// Get the value of the given column
			return res.getObject(col+1);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		// If a problem arrises, just return a blank string
		return "";
	}
	
	/*
	 * Perform an SQL query to fill the result set with data
	 * 
	 * @param query the SQL query to run on the database
	 */
	public void setQuery (String query) throws SQLException, IllegalStateException {
		if (!connectedToDatabase) 	// If not connected, throw an exception
			throw new IllegalStateException("Not connected to Database");
		// Execute the query
		res = stmt.executeQuery(query);
		// Get the meta data
		meta = res.getMetaData();
		// Move to the last row of the result
		res.last();
		// and get the row number, this is the number of rows
		numRows = res.getRow();
		// Let the table know the model has changed
		fireTableStructureChanged();
	}
	
	/*
	 * Nice and tidy, let us clean everything up
	 */
	public void disconnectFromDatabase () {
		if (connectedToDatabase) {	// If we have a connection
			try {
				// Close the resultset
				res.close();
				// close the statement
				stmt.close();
				// and close the connection
				con.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} finally {
				// And we are no longer connected
				connectedToDatabase = false;
			}
		}
	}
}