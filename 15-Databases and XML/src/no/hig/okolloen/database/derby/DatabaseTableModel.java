package no.hig.okolloen.database.derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.table.AbstractTableModel;

/*
 * Much the same as the last table model, but this model let the user update the data in the database as well.
 */
@SuppressWarnings("serial")
public class DatabaseTableModel extends AbstractTableModel {
	// The location of the database
	private final static String url = "jdbc:derby:MyDbTest";
	// The one and only query, show all information about the contacts
	static final String DEFAULT_QUERY = "SELECT * FROM contacts";
	private Connection con;
	private Statement stmt;
	private ResultSet res;
	private ResultSetMetaData meta;
	private int numRows;
	private boolean connectedToDatabase = false;
	
	public DatabaseTableModel () throws SQLException{
		// Establish the connection to the database
		con = DriverManager.getConnection(url);
		// Create a statement, this time we want BOTH to be able to scroll back and forth AND to be able to update the database
		// through the result set
		// This means we will do a SELECT query but use the result to perform an implicit UPDATE query
		stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		connectedToDatabase = true;
		setQuery (DEFAULT_QUERY);
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) throws IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		try {
			String className = meta.getColumnClassName(columnIndex+1);
			return Class.forName(className);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Object.class;
	}
	
	@Override
	public int getColumnCount() throws IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		try {
			return meta.getColumnCount();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return 0;
	}

	@Override
	public String getColumnName(int column) throws IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		try {
			return meta.getColumnName(column+1);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return "";
	}
	
	@Override
	public int getRowCount() throws IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		return numRows;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return (columnIndex>0);
	}
	
	@Override
	public Object getValueAt(int row, int col) throws IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		try {
			res.absolute(row+1);
			return res.getObject(col+1);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
		return "";
	}
	
	/*
	 * This is the only other difference from the previous table model.
	 * Used to set the value of a row/col in the database table.
	 * 
	 * NOTE, this is only possible if the SELECT is on a single table.
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object val, int row, int col) {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		try {
			// Move to the right row
			res.absolute(row+1);
			// Update the value in the given column
			res.updateObject(col+1, val);
			// Fire the update back at the database
			// NOTE, can be buffered, important to close the result set/connection 
			// correctly to ensure that the database gets updated.
			res.updateRow();
			// Let the table know the model has changed
			fireTableCellUpdated(row, col);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
	
	public void setQuery (String query) throws SQLException, IllegalStateException {
		if (!connectedToDatabase) 
			throw new IllegalStateException("Not connected to Database");
		res = stmt.executeQuery(query);
		meta = res.getMetaData();
		res.last();
		numRows = res.getRow();
		fireTableStructureChanged();
	}
	
	public void disconnectFromDatabase () {
		if (connectedToDatabase) {
			try {
				res.close();
				stmt.close();
				con.close();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} finally {
				connectedToDatabase = false;
			}
		}
	}
}