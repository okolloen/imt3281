package no.hig.okolloen.database.derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.sql.PreparedStatement;

/*
 * Databases is so much more fun when they contain data, we have a lot of contact information in the MySQL database.
 * Lets transfer it to our local database
 */
public class FillTable {
	// The URL for the local Derby database
	private final static String url = "jdbc:derby:MyDbTest";
	// This is the URL for the MySQL database
	private final static String extURL = "jdbc:mysql://stan.hig.no/imt3281-eks";
	private static final String USERNAME = "imt3281-eks";
	private static final String PASSWORD = "imt3281";
	
	public static void main(String[] args) {
		try {
			// Connect to the local Derby database
			Connection con =  DriverManager.getConnection(url);
			// We will be performing an INSERT statement over and over, create it as an prepares statement
			PreparedStatement stmt = con.prepareStatement("INSERT INTO contacts "+
					"(givenname, surename, email, phone) "+
					"VALUES (?, ?, ?, ?)");
			
			// Connect to the MySql database
			Connection extCon = DriverManager.getConnection(extURL, USERNAME, PASSWORD);
			// Create a statement
			Statement extStmt = extCon.createStatement();
			// Get firstname lastname, email and phone number from the MySQL database
			// but only get the employees, not the students
			ResultSet extRes = extStmt.executeQuery("SELECT givenname, surename, email, phone FROM users WHERE employee='y'");
			
			// Create a statement on the local database
			Statement delete = con.createStatement();
			// and delete the existing contacts
			delete.execute("DELETE FROM contacts");
			
			// Keep a count of the number of contacts imported
			int counter = 0;
			
			// Loop through the result from the MySQL database
			while (extRes.next()) {
				// Set the values in the prepared statement
				stmt.setString(1, extRes.getString(1));
				stmt.setString(2, extRes.getString(2));
				stmt.setString(3, extRes.getString(3));
				stmt.setString(4, extRes.getString(4));
				// Execute the prepared statement (insert a new row in the local database)
				counter += stmt.executeUpdate();
			}
			// Close both connections
			con.close();
			extCon.close();
			
			System.out.printf("%d contacts imported to DB", counter);
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}
