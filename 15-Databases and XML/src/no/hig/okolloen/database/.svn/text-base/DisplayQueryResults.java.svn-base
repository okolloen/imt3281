package no.hig.okolloen.database;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

@SuppressWarnings("serial")
public class DisplayQueryResults extends JFrame {
	static final String DATABASE_URL = "jdbc:mysql://stan.hig.no/imt3281-eks";
	static final String USERNAME = "imt3281-eks";
	static final String PASSWORD = "imt3281";
	static final String DEFAULT_QUERY = "SELECT * FROM users";
	
	private ResultSetTableModel tableModel;
	private JTextArea queryArea;
	private JTable resultTable;
	
	/*
	 * A nice GUI to perform SQL SELECT queries and display the results.
	 * Use a textarea to enter the query and a jtable with our resultsettablemodel to display the results
	 */
	public DisplayQueryResults () {
		super ("Displaying query results");
		try {
			// Set up the table model
			tableModel = new ResultSetTableModel(DATABASE_URL, USERNAME, PASSWORD, DEFAULT_QUERY);
			
			// Set up the text area
			queryArea = new JTextArea (DEFAULT_QUERY, 3, 30);
			// We want word wrapping if the lines become to long
			queryArea.setWrapStyleWord(true);
			queryArea.setLineWrap(true);
			
			// Put a scroll pane around the text area, never use horizontal scroll bars
			// and only use vertical scroll bars when needed
			JScrollPane scrollPane = new JScrollPane(queryArea,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, 
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			JButton submitButton = new JButton ("Submit Query");
			JPanel top = new JPanel();
			top.setLayout(new BorderLayout());
			top.add(scrollPane, BorderLayout.CENTER);
			top.add(submitButton, BorderLayout.EAST);
			// We have a panel with a text area and a button, add this to the top of the screen
			add (top, BorderLayout.NORTH);
			
			// Create the table to hold the results
			resultTable = new JTable(tableModel);
			// Add the table to a scroll pane in the center of the screen
			add (new JScrollPane(resultTable), BorderLayout.CENTER);
			
			// When the submit button is pressed we want to perform a new query
			submitButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						// Attempt to run the query
						tableModel.setQuery(queryArea.getText());
					} catch (SQLException sqle) {
						// If an error is returned, then show the message and 
						JOptionPane.showMessageDialog(DisplayQueryResults.this, sqle.getMessage(), "Database error", JOptionPane.ERROR_MESSAGE);
						try {	// try to perform our standard query
							tableModel.setQuery(DEFAULT_QUERY);
							queryArea.setText(DEFAULT_QUERY);
						} catch (SQLException sqle2) {
							// If still an error, show error message
							JOptionPane.showMessageDialog(DisplayQueryResults.this, sqle2.getMessage(), "Database error", JOptionPane.ERROR_MESSAGE);
							// This probably means that there's something seriously wrong
							tableModel.disconnectFromDatabase();
							// so end the program
							System.exit(1);
						}
					}
				}
			});
		} catch (SQLException sqle) {	// This is caused by the constructor of the table model failing
			JOptionPane.showMessageDialog(DisplayQueryResults.this, sqle.getMessage(), "Database error", JOptionPane.ERROR_MESSAGE);
			// Nothing much we can do without the table model working
			tableModel.disconnectFromDatabase();
			// So just exit
			System.exit(1);
		}
		// Why not let the table be able to sort on the different columns :)
		final TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
		resultTable.setRowSorter(sorter);
		
		// We want to disconnect from the database before we exit
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				// So when the window is closed, close the connection
				tableModel.disconnectFromDatabase();
				// and exit
				System.exit(0);
			}
		});
		pack();
		setVisible(true);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new DisplayQueryResults();
	}

}
