package no.hig.okolloen.xml.valuta;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 * Program fetches updated currency exchange rates from DNB. http://www.dnbnor.no/portalfront/datafiles/miscellaneous/csv/kursliste_ws.xml
 * The XML is turned into a DOM and that DOM is then parsed to extract the information.
 * The user is presented with a drop down box that lets him/her select a currency and display more details about that currency
 */
@SuppressWarnings("serial")
public class ValutaKurser extends JPanel {
	// A collection of currency exchange rates
	ArrayList<Valuta> valuta = new ArrayList<Valuta>();
	// The above collection is transfered to this drop down box once complete
	JComboBox<Valuta> kurser = new JComboBox<Valuta>();
	// Label used to display details about a currency
	JLabel country, isocode, code, unit, name, middle, buy, sell;
	
	/*
	 * The constructor, reads the XML file from the bank and builds up the internal data structure
	 */
	public ValutaKurser () {
		try {
			// We create a document builder factory
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			// Use the document builder factory to create a document builder
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			// Use the document builder to read and parse the XML file into DOM format
			Document doc = dBuilder.parse("http://www.dnb.no/portalfront/datafiles/miscellaneous/csv/kursliste_ws.xml");
			// Find all nodes in the DOM with the tag <valutakurs>
			NodeList list = doc.getElementsByTagName("valutakurs");
			// Go through the list of nodes
			for (int i=0; i<list.getLength(); i++) {
				// Create a new object containing currency exchange information and add it to the array list 
				valuta.add(new Valuta(list.item(i)));
			}
			System.out.printf ("%d valutakurser funnet", valuta.size());
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException saxe) {
			saxe.printStackTrace();
		}
		// Convert the array list to an array of currency information objects, use that array
		// to create a default model for the drop down box and set it as the drop down boxs data
		kurser.setModel(new DefaultComboBoxModel<Valuta> (valuta.toArray(new Valuta[1])));
		setLayout (new BorderLayout());
		add (kurser, BorderLayout.NORTH);
		// When a new currency exchange item is selected
		kurser.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Show the details about that currency exchange object
				showDetails ((Valuta)kurser.getSelectedItem());
			}
		});
		// Adds a bunch of JLabel objects to create a detailed view of currency information 
		setupDetailDisplay();
	}

	/*
	 * Used to display details about a currency exchange object
	 */
	protected void showDetails(Valuta item) {
		country.setText(item.country);
		isocode.setText(item.isocode);
		code.setText(item.code);
		unit.setText(String.format("%d", item.unit));
		name.setText(item.name);
		middle.setText(String.format ("%f",item.middle));
		buy.setText(String.format ("%f",item.buy));
		sell.setText(String.format("%f", item.sell));
	}

	/*
	 * adds a panel with labels controlled by a gridbaglayout object
	 * This is used to display details about a currency exchange item
	 */
	private void setupDetailDisplay() {
		JPanel details = new JPanel ();
		details.setLayout(new GridLayout(0, 2));
		JLabel land = new JLabel("Land: ", JLabel.RIGHT);
		details.add(land);
		details.add(country = new JLabel(" "));
		JLabel isokode = new JLabel("Isokode: ", JLabel.RIGHT);
		details.add(isokode);
		details.add(isocode = new JLabel (" "));
		JLabel kode = new JLabel("Kode: ", JLabel.RIGHT);
		details.add(kode);
		details.add(code = new JLabel(" "));
		JLabel enhet = new JLabel("Enhet: ", JLabel.RIGHT);
		details.add(enhet);
		details.add(unit = new JLabel(" "));
		JLabel navn = new JLabel("Navn: ", JLabel.RIGHT);
		details.add(navn);
		details.add(name = new JLabel (" "));
		JLabel kurs = new JLabel ("Kurs: ", JLabel.RIGHT);
		details.add(kurs);
		details.add(middle = new JLabel(" "));
		JLabel kjop = new JLabel("Kjøp: ", JLabel.RIGHT);
		details.add(kjop);
		details.add(buy = new JLabel(" "));
		JLabel salg = new JLabel("Salg: ", JLabel.RIGHT);
		details.add(salg);
		details.add(sell = new JLabel(" "));
		add (details, BorderLayout.CENTER);
	}
	
	public static void main(String[] args) {
		JFrame f = new JFrame ("Valutakurser");
		ValutaKurser vk = new ValutaKurser();
		f.add (vk);
		f.pack ();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

}
