package no.hig.okolloen.xml.valuta;

import org.w3c.dom.Node;

/*
 *  Class used to keep key information about currency exchange.
 *  The values get initialized from the exchange rates found at : 
 *  http://www.dnbnor.no/portalfront/datafiles/miscellaneous/csv/kursliste_ws.xml
 *  The node is the content of the <valutakurs>..</valutakurs>
 */

public class Valuta {
	String country, isocode, code, name;
	int	unit;
	float buy, sell, middle;
	
	/*
	 * Initialize a currency exchange rate object.
	 * Takes a node from the xml document fetched from :
	 * http://www.dnbnor.no/portalfront/datafiles/miscellaneous/csv/kursliste_ws.xml 
	 * as a parameter
	 */
	public Valuta (Node valutakurs) {
		// Collapse double empty into single empty nodes 
		valutakurs.normalize();
		// Walk through all child nodes.
		for (int i=0; i<valutakurs.getChildNodes().getLength(); i++) {
			// Fetch each child node of the <valutakurs> nodes
			Node n = valutakurs.getChildNodes().item(i);
			// The first few is plain nodes
			if (n.getNodeName().equals("land"))
				country = n.getTextContent();
			else if (n.getNodeName().equals("isokode"))
				isocode = n.getTextContent();
			else if (n.getNodeName().equals("kode"))
				code = n.getTextContent();
			else if (n.getNodeName().equals("enhet"))
				unit = Integer.parseInt(n.getTextContent());
			else if (n.getNodeName().equals("navn"))
				name = n.getTextContent();
			// The overforsel node contains high, low and middle nodes, we only want the middle one
			else if (n.getNodeName().equals("overforsel")) {
				for (int ii=0; ii<n.getChildNodes().getLength(); ii++) {
					Node o = n.getChildNodes().item(ii);
					if (o.getNodeName().equals("midtkurs"))
						middle = Float.parseFloat(o.getTextContent());
				}
			// Same thing with seddel, it contains kjop and salg, we want to keep both
			} else if (n.getNodeName().equals("seddel")) {
				for (int ii=0; ii<n.getChildNodes().getLength(); ii++) {
					Node o = n.getChildNodes().item(ii);
					if (o.getNodeName().equals("kjop"))
						buy = Float.parseFloat(o.getTextContent());
					else if (o.getNodeName().equals("salg"))
						sell = Float.parseFloat(o.getTextContent());
				}
			}
		}
	}
	
	/*
	 * The result of the toString method is shown in a drop down box, allowing us to select the currency 
	 * we want to investigate further.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString () {
		return String.format ("%s (%s) %2.1f", country, name, middle);
	}
}
