package no.hig.okolloen.xml.weatherXML;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/*
 * Reads the forecast data as XML, creating a DOM and using
 * DOM operations to extract the weather forecast information that we want to display.
 * 
 * The XML file contains among other data a summary forecast for the next coming days.
 * We extract information about which days and the summary text forecast.
 */
@SuppressWarnings("serial")
public class ShowWeatherXML extends ShowWeather {
	public ShowWeatherXML () {
		// Keep everything from the last example
		super ();
		// But when a place is selected we want to do this as well
		steder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// An arraylist with the names of the days the forecast i valid for
				ArrayList<String> days = new ArrayList<String>();
				// An arraylist with the forecast for the days above
				ArrayList<String> forecasts = new ArrayList<String>();
				try {
					// Need to get a document builder factory to get a document builder
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					// A document builder is used to parse an XML document and generate a document object model
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					// Read and parse the XML document at the given URL
					Document doc = dBuilder.parse(((Sted)steder.getSelectedItem()).getXmlURL());
					// Locate all nodes with the tag name "text"
					NodeList list = doc.getElementsByTagName("text");
					// There should be only one such node, but we loop over the result just in case
					for (int i=0; i<list.getLength(); i++) {
						// Extract the i'th node from the result
						Node n = list.item(i);
						// Remove extra empty nodes, note, there will still be empty nodes, 
						// just not double empty nodes
						n.normalize();
//						System.out.println(n.getTextContent());
						// n is the text node, the first child is a blank, the next sibling is the location node
						// and we want the children of that node
						// <text>
						//   <location>
						//     <time....>
						//       <title>day of week</title>
						//		 <body>The forecast</body>
						//     </time>
						//     <time....> ....
						//     </time>
						// We should be getting the <time...> ... </time> nodes
						NodeList times = n.getFirstChild().getNextSibling().getChildNodes();
						for (int idx=0; idx<times.getLength(); idx++) {
							// We have one empty node inbetween each time node
							// The empty nodes have nodeName #text
							if (times.item(idx).getNodeName().equals("time")) {
								// Fetch the title and body nodes
								NodeList weather = times.item(idx).getChildNodes();
								// Loop through the nodes, note we also have some empty nodes
								// this way we get what we want anyway
								for (int ii=0; ii<weather.getLength(); ii++) {
									// If this is a node with tag title
									if (weather.item(ii).getNodeName().equals("title")) {
										// Get the text content of this node and add it to the days arraylist
										days.add(weather.item(ii).getTextContent());
									// else, if this is a node with tag body
									} else if (weather.item(ii).getNodeName().equals("body")) {
										// Get the forecast and att it to the forecasts arraylist
										forecasts.add (weather.item(ii).getTextContent());
									} // If not title/body just skip it
								}
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				// Create a new panel
				JPanel p = new JPanel ();
				// Set layout with 2 columns and as many rows as we have days with forecast
				p.setLayout(new GridLayout(days.size(),2));
				int i=0; 
				// Loop through the days
				for (String tmp : days) {
					// Add a label with the name of the day(s)
					p.add(new JLabel (tmp));
					// Add the forecast to a label (inside complete html) and add that label to a scrollpane
					JScrollPane sp = new JScrollPane(new JLabel (String.format("<html><body>%s</body></html>",forecasts.get(i))));
					// This is what controls the final size of the layout
					// It doesn't look to good, but it's quick and easy
					sp.setPreferredSize(new Dimension(300,40));
					p.add (sp);
					// Need to keep a count to be able to fetch the right forecast
					i++;
					
				}
				// Use JOptionPane to show a dialog with the weather forecast.
				JOptionPane.showMessageDialog(null, p, "Værvarsel for de neste dager", JOptionPane.PLAIN_MESSAGE);
			}
		});
	}
	
	public static void main(String[] args) {
		ShowWeatherXML weather = new ShowWeatherXML();
		JFrame f = new JFrame();
		f.add (weather);
		f.pack();
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
