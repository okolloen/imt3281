package no.hig.okolloen.xml.weatherXML;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/*
 * Reads in a list of places and creates a window where you can select the county, then the municipality and lastly the place.
 * When a place is selected the XML file for that place is displayed in a JTextArea.
 * 
 * The file noreg.txt is downloaded from http://om.yr.no/verdata/xml/
 */
@SuppressWarnings("serial")
public class ShowWeather extends JPanel {
	// A list of all the places we have found
	ArrayList<Sted> places = new ArrayList<Sted>();
	// A list of all the counties, the counties holds the municipalities which in turn holds the places 
	ArrayList<Fylke> counties = new ArrayList<Fylke>();
	// Drop down box to select county
	JComboBox<Fylke> fylker = new JComboBox<Fylke> ();
	// Drop down box to select municipality, each municipality will hold all places in that municipality
	JComboBox<Kommune> kommuner = new JComboBox<Kommune>();
	// Drop down box to select the place to get the weather forecast for
	JComboBox<Sted> steder = new JComboBox<Sted>();
	// A text area used to show the downloaded weather forecast
	JTextArea weatherForecast = new JTextArea (50, 80);
	
	/*
	 * The constructor, sets up the layout and loads the file with the information about places, municipalities and counties.
	 */
	public ShowWeather () {
		// Loads the file containing all initial information. Fills the county drop down box
		loadPlaces();
		System.out.printf("%d places found.", places.size());
		// Initialize the layout
		weatherLayout();
	}

	/*
	 * Handle all layout and sets up actionListeners on the three drop down boxes
	 */
	private void weatherLayout() {
		setLayout (new BorderLayout());
		JPanel chooser = new JPanel ();
		// The layout is :
		// Label: dropdownbox
		// Label: dropdownbox
		// Label: dropdownbox
		chooser.setLayout(new GridLayout(3, 2));
		// The counties arraylist contains all the counties, convert it to an array of items of class Fylke
		// Make a new model based on that array and set that model as the model for the first 
		// drop down box (counties)
		this.fylker.setModel(new DefaultComboBoxModel<Fylke>(counties.toArray(new Fylke[1])));
		// Right align the label to make the layout nice and tidy
		JLabel selectFylke = new JLabel("Velg fylke: ", JLabel.RIGHT);
		chooser.add (selectFylke);
		chooser.add (this.fylker);
		// When a new county is selected we want to fill in the municipality drop down box and 
		// clear the places drop down box (can't find a place before we have selected the municipality.)
		this.fylker.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Fetch the arraylist of municipalities from the selected county
				// Convert it to an array of class Kommune, use that array to initialize 
				// a defaultComboBoxModel and then set that model as the data for selecting 
				// a municipality
				kommuner.setModel(new DefaultComboBoxModel<Kommune>(
						((Fylke)fylker.getSelectedItem()).getKommuner().toArray(new Kommune[1])));
				// Clears the drop down box of places
				steder.setModel(new DefaultComboBoxModel<Sted>());
			}
		});
		JLabel selectKommune = new JLabel ("Velg kommune: ", JLabel.RIGHT);
		chooser.add (selectKommune);
		chooser.add (this.kommuner);
		// When a municipality is selected we want to fill the drop down box with the places
		// from that municipality
		this.kommuner.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Fetch an arraylist of places from the selected municipality
				// Then convert it to an array of class "Sted", use that array
				// to initialize a defaultComboBoxModel and use that model
				// as data for the places drop down box
				steder.setModel(new DefaultComboBoxModel<Sted>(
						((Kommune)kommuner.getSelectedItem()).getSteder().toArray(new Sted[1])));
			}
		});
		JLabel selectSted = new JLabel("Velg sted: ", JLabel.RIGHT);
		chooser.add (selectSted);
		chooser.add (this.steder);
		// When a place is selected we want to load the weather forecast for that place
		this.steder.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Load the forecast
				loadForecast();
			}
		});
		add (chooser, BorderLayout.NORTH);
		// Add the text area to a scroll pane
		add (new JScrollPane(weatherForecast), BorderLayout.CENTER);
	}

	/*
	 * Method used to load the initial data and fill in the data structure used in the program
	 */
	private void loadPlaces() {
		try {
			// Read it as a textfile encoded with utf-8
			BufferedReader br = new BufferedReader (
					new InputStreamReader (getClass().getResourceAsStream("/stedsinfo/noreg.txt"), "utf-8"));
			String tmp = br.readLine();	// Throw away the header line
			// Read one place at a time
			while ((tmp=br.readLine())!=null) {
				// The file is "mostly" tab separated
				String data[] = tmp.split("[\t]");
				// Create a new place
				Sted sted = new Sted ();
				sted.setStedsnavn(data[1]);
				
				// This tells us what kind of place this is, church, city center etc.
				sted.setStedstype(data[4]);
				
				// Check to see if this place is in a county that have been previously registered
				Fylke fylke = new Fylke ();
				fylke.setFylkesnavn(data[7]);
				if (counties.contains(fylke)) {		// The arrayList of counties already has this county registered
					// Get the index of the county and then fetch the previously registered county
					fylke = counties.get(counties.indexOf(fylke));
					// Check to see if the place is in a municipality that have been previously registered
					Kommune kommune = new Kommune();
					kommune.setKommunenavn(data[6]);
					if (fylke.getKommuner().contains(kommune)) {	// The arraylist of municipalities has this municipality registeder
						// Get the index of this municipality and then fetch the previusly registered municipality
						kommune = fylke.getKommuner().get(fylke.getKommuner().indexOf(kommune));
						// System.out.printf("Kommunen har %d steder\n", kommune.getSteder().size());
						// Add this place to the registered municipality
						kommune.getSteder().add(sted);
					} else {
						// System.out.println("Legg til sted");
						// Add this place to a new municipality 
						kommune.getSteder().add(sted);
						// Add the municipality to a registered county
						fylke.getKommuner().add(kommune);
					}
				} else {
					// The county has not been previously registered
					// Create a new municipality
					Kommune kommune = new Kommune();
					kommune.setKommunenavn(data[6]);
					// Add this place to the municipality
					kommune.getSteder().add(sted);
					// Add the municipality to the county
					fylke.getKommuner().add(kommune);
					// Add the county to the list of counties
					counties.add(fylke);
				}
				// Fill in the rest of the data for the place
				sted.setLat(Float.parseFloat(data[8]));
				sted.setLng(Float.parseFloat(data[9]));
				sted.setXmlURL(data[12]);
				// We also keep a list of all places
				// Only used to count the total number of places
				places.add (sted);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	/*
	 * Load the forecast for a given place
	 */
	private void loadForecast() {
		try {
			// Wrap the text version of the URL for the file into a URL object, then open that stream
			// read it as a utf-8 encoded text file
			BufferedReader br = new BufferedReader (new InputStreamReader(
					new URL (((Sted)steder.getSelectedItem()).getXmlURL()).openStream(), "utf-8"));
			String tmp;
			// Clear the current forecast
			weatherForecast.setText("");
			while ((tmp = br.readLine())!=null) {
				// Then append each line as they are read from the file
				// This is safe since it happens in the GUI update thread
				// Note, it blocks the update of the GUI, but the file should
				// be fetched rather quickly
				weatherForecast.append(tmp);
				weatherForecast.append("\n");
			}
		} catch (MalformedURLException murle) {
			murle.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		// We want to display the text from the beginning, but
		// for that to happen correctly this need to happen after 
		// the text area is updated completely
		// So, we put this on the que for execution after all pending updates have been performed
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				weatherForecast.setCaretPosition(0);
			}
		});
	}

	/*
	 * Start it all up.
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame ("Værvarslingen");
		ShowWeather weather = new ShowWeather();
		f.add (weather);
		f.pack();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}