package no.hig.okolloen.xml.weatherXML;

import java.util.ArrayList;

/*
 * This class holds the name of the municipality together with all registered places in 
 * that municipality. 
 */
public class Kommune {
	String kommunenavn;
	ArrayList<Sted> steder = new ArrayList<Sted>();
	
	/*
	 * Two municipality objects is said to be equal if they refer to the same
	 * municipality name.
	 * 
	 * @param obj the object we want to check if is referring to the the same municipality
	 * @return true if the two objects is refering to the same municipality  
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// This can only be if the object is indeed a municipality
		if (obj instanceof Kommune)
			return kommunenavn.equals(((Kommune)obj).getKommunenavn());
		return false;
	}
	
	// The usual getter and setter methods
	public String getKommunenavn() {
		return kommunenavn;
	}
	public void setKommunenavn(String kommunenavn) {
		this.kommunenavn = kommunenavn;
	}
	public ArrayList<Sted> getSteder() {
		return steder;
	}

	/*
	 * The result of the toString method is used in a drop down box used to select municipality
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString () {
		return getKommunenavn();
	}
}
