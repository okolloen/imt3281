package no.hig.okolloen.xml.weatherXML;

/*
 * This class is used to hold information about a place where we have available weather forecast data.
 * Each place has a name and a type and belong to a municipality and a county. 
 * We also have information about the center position of the place.
 * Lastly we have an URL to a file containing XML formated information about the weather.
 */
public class Sted {
	String stedsnavn;
	String stedstype;
	Kommune kommune;
	Fylke fylke;
	float lat;
	float lng;
	String xmlURL;
	
	// All the usual getter and setter methods
	public String getStedsnavn() {
		return stedsnavn;
	}
	public void setStedsnavn(String stedsnavn) {
		this.stedsnavn = stedsnavn;
	}
	public String getStedstype() {
		return stedstype;
	}
	public void setStedstype(String stedstype) {
		this.stedstype = stedstype;
	}
	public Kommune getKommune() {
		return kommune;
	}
	public void setKommune(Kommune kommune) {
		this.kommune = kommune;
	}
	public Fylke getFylke() {
		return fylke;
	}
	public void setFylke(Fylke fylke) {
		this.fylke = fylke;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLng() {
		return lng;
	}
	public void setLng(float lng) {
		this.lng = lng;
	}
	public String getXmlURL() {
		return xmlURL;
	}
	public void setXmlURL(String xmlURL) {
		this.xmlURL = xmlURL;
	}
	
	/*
	 * The return value of the toString method is used to represent the place in a drop down box showing available locations.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString () {
		return String.format ("%s (%s)", getStedsnavn(), getStedstype());
	}
}