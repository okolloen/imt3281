package no.hig.okolloen.xml.weatherXML;

import java.util.ArrayList;

/*
 * Objects of this class is used to hold a collection of the municipalities in a county.
 * 
 */
public class Fylke {
	String fylkesnavn;
	ArrayList<Kommune> kommuner = new ArrayList<Kommune>();
	
	/*
	 * Two object is said to be equal if they refer to the same county name
	 * 
	 * @param obj an object to check if it is referring to the same county as this object
	 * @return true if the two objects refer to the same county
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// This can only be so if the object is actually a county
		if (obj instanceof Fylke)
			return fylkesnavn.equals(((Fylke)obj).getFylkesnavn());
		else
			return false;
	}
	
	// The usual getter and setter methods
	public String getFylkesnavn() {
		return fylkesnavn;
	}
	public void setFylkesnavn(String fylkesnavn) {
		this.fylkesnavn = fylkesnavn;
	}
	public ArrayList<Kommune> getKommuner() {
		return kommuner;
	}
	
	/*
	 * The result of the toString method is used to show a drop down box of counties.
	 * 
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString () {
		return getFylkesnavn();
	}
}
