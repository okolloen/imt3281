package no.hig.okolloen.xml.saxParser;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

/*
 * Demonstrates a JDesktopPane with a wallpaper background.
 * The wallpaper is fetched from a flickr stream where we search for the tags beach and sunset.
 * The wallpaper is exchanged every ten seconds just to demonstrate the effect.
 * Also, the wallpapers is scaled to fill the exact size of the desktop pane.
 */
@SuppressWarnings("serial")
public class MyDecoratedDesktopPane extends JDesktopPane {
	// A list of URLs to the images to display
	ArrayList<URL> images = new ArrayList<URL>();
	// Reference to the currently shown image 
	Image img = null;
	
	/*
	 * Constructor for this desktop pane.
	 * Reads the flickr stream and finds all images then set up the image switching routine.
	 */
	public MyDecoratedDesktopPane () {
		super ();
		// Need this, if not it would always be grey anyway.
		setOpaque(false);
		// Read and parse the flickr stream
		parseDocument();
		// Start of by showing the last image we fetched
		img = new ImageIcon (images.get(images.size()-1)).getImage();
		// Then start up the thread that will cycle through all the images over and over again, 
		// starting with the first image
		Thread imageSwitcher = new Thread () {
			public void run() {
				// Loop forever
				while (true) {
					// Go through the images one at a time
					for (URL image : images) {
						try {
							// Wait for 10 seconds
							Thread.sleep (10000);
							// Then fetch the next image
							img = new ImageIcon (image).getImage();
							// Repain the desktop pane to show the new image
							MyDecoratedDesktopPane.this.repaint ();
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}
					}
				}
			}
		};
		// Start this thread, it will be mostly dormant
		imageSwitcher.setDaemon(true);
		imageSwitcher.start();
	}

	/*
	 * Calling repaint means that paint will be called eventually.
	 * The paint method will also be called whenever anything changes as to merit a repaint 
	 * of the desktop pane.
	 * 
	 * (non-Javadoc)
	 * @see javax.swing.JLayeredPane#paint(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		// The graphics object is somewhat constrained in its abilities
		// Much more can be achieved by using the Graphics2D object
		// i.e. smoother scaling :)
		Graphics2D g2d = (Graphics2D)g;
		// Set up a transform
		AffineTransform transform = new AffineTransform();
		// If we can get the height of the image
		if (img.getHeight(this)>-1) {
			// Then get the height and width
			int height = img.getHeight(this);
			int width = img.getWidth(this);
			// And calculate the scale factor for height and with to get
			// the image displayed scaled to fill the desktop pane
			transform.setToScale(getWidth()/(float)width, getHeight()/(float)height);
		}
		// Paint the image, if we couldn't figure out the height of the image
		// then just paint the image at 1/1 ratio
		g2d.drawImage(img, transform, this);
		// And then paint everything else on the desktop pane
		// i.e. all the child windows
		super.paint(g);
	}
	
	/*
	 * Read the flickr feed and extract the images as we go
	 */
	private void parseDocument() {
		//get a factory for the parsers
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {

			//get a new instance of parser
			SAXParser sp = spf.newSAXParser();

			// parse the file and also register this class for call backs
			// the FlickrImageFinderParser will receive notifications each time 
			// the parser finds a new node, reaches the end of a node etc.
			// Give the callback object a reference to the array of images.
			sp.parse("http://api.flickr.com/services/feeds/photos_public.gne?tags=beach,sunset", 
					new FlickrImageFinderParser(images));

		}catch(SAXException se) {
			se.printStackTrace();
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch (IOException ie) {
			ie.printStackTrace();
		}
	}
	
	// Just create a frame with this desktop pane
	public static void main(String[] args) {
		JFrame f = new JFrame("Testing my great decorated desktop");
		f.add(new MyDecoratedDesktopPane());
		f.setSize(800, 600);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
