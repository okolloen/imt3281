package no.hig.okolloen.xml.saxParser;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/*
 * A callback object for the sax parser.
 */
public class FlickrImageFinderParser extends DefaultHandler {
	ArrayList<URL> images = new ArrayList<URL>();
	
	/*
	 * Receives a reference to an arraylist to store the image URL's that it finds
	 */
	public FlickrImageFinderParser (ArrayList<URL> images) {
		this.images = images;
	}
	
	/*
	 * We only need to handle the start of elements as all the information we need is available the the start 
	 * of an element. We are looking for elements with tag name "link" and attribute rel=enclosure.
	 * Then in the attribute href we find the link to the image.
	 * 
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// Make sure the underlying things still works
		super.startElement(uri, localName, qName, attributes);
		// The tag name is in the gName attribute to this method
		if (qName.equals("link")) {
			try {
				// Check to see if this link tag has a attribute of "rel", and if so
				// is the value of that attribute = "enclosure"
				if ((attributes.getValue("rel")!=null)&&(attributes.getValue("rel").equals("enclosure"))) {
					// System.out.println(attributes.getValue("href"));
					// If so, add this as a new URL to the image list
					images.add(new URL(attributes.getValue("href")));
				}
			} catch (MalformedURLException murle) {
				murle.printStackTrace();
			}
		}
	}
}