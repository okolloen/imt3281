package no.hig.imt3281.lambda;
interface Operation {
	// Single method interface, functional interface
	public int perform (int x, int y);
}

class Runner {
	public void run (Operation o) {
		System.out.println("The sum is " + o.perform(12, 15));
	}
}

public class Ex02Basics {
	public static void main(String[] args) {
		Runner runner = new Runner();
		// The old way
		runner.run(new Operation () {
			public int perform (int x, int y) {
				System.out.println("adding "+x+" to "+y);
				return x+y;
			}
		});
		
		runner.run((a, b) -> {
			System.out
					.println("adding " + a + " to " + b + " using lambdas.");
			return a+b;
		});
	}
}
