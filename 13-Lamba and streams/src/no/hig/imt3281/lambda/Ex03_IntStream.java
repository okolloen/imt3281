package no.hig.imt3281.lambda;

import java.util.function.IntConsumer;
import java.util.stream.IntStream;
// Java How to program 10th edition, fig 17.5

public class Ex03_IntStream {
	public static void main(String[] args) {
		int values[] = { 3, 10, 6, 1, 4, 8, 2, 5, 9, 7 };
		
		System.out.println("Original values");
		IntStream.of(values).forEach(value -> System.out.printf ("%d ", value));
		// Count, min, max, sum and average
		System.out.printf("\nCount: %d\tMin: %d\tMax: %d\tSum: %d\tAverage: %.2f\n", 
                                   IntStream.of(values).count(), IntStream.of(values).min().getAsInt(), 
                                   IntStream.of(values).max().getAsInt(), IntStream.of(values).sum(),
                                   IntStream.of(values).average().getAsDouble());
		
		// Reducing the array
		System.out.printf("Sum via reduce method: %d\n", 
				IntStream.of(values).reduce (0, (x, y) -> x + y));
		System.out.printf("Sum of squares via reduce method: %d\n", 
				IntStream.of(values).reduce (0, (x, y) -> x + y*y));
		System.out.printf("Product via reduce method: %d\n", 
				IntStream.of(values).reduce (1, (x, y) -> x * y));
		
		// Filtering and sorting
		System.out.println("Even values in sorted order: ");
		IntStream.of(values).filter(value -> value % 2 ==0)
										.sorted()
										.forEach(value -> System.out.printf("%d ", value));
		System.out.println();
		System.out.println("Odd values multiplied by 10 in sorted order: ");
		IntStream.of(values).filter(value -> value % 2 !=0)
										.map(value -> value * 10)
										.sorted()
										.forEach(value -> System.out.printf("%d ", value));
		System.out.println();
		
		// Range limiting
		System.out.println("Range, exclusive");
		IntStream.range(1, 10).forEach(value -> System.out.printf("%d ", value));
		System.out.println("\nRange, inclusive");
		IntStream.rangeClosed(1, 10).parallel().forEach(value -> System.out.printf("%d ", value));
	}
}
