package no.hig.imt3281.lambda;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Ex07_StreamOfLines {
	public static void main(String[] args)  throws IOException {
		Pattern pattern = Pattern.compile("\\s+");
		
		URL u = new URL ("http://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html");
				
		// Read from the URL, replace punctuation, split to separate words and finally count the occurrences of the words
		Map<String, Long> wordCounts =  new BufferedReader(new InputStreamReader(u.openStream()))
				.lines()
				.map(line -> line.replaceAll("(?!')\\p{P}", ""))
				.flatMap(line -> pattern.splitAsStream (line))
				.collect(Collectors.groupingBy(String::toLowerCase, TreeMap::new, Collectors.counting()));
		
		// Remove "blank" words, group by first letter, then display.
		wordCounts.entrySet()			
				.stream()
				.filter(entry -> entry.getKey().length()>0)
				.collect(
						Collectors.groupingBy(entry -> entry.getKey().charAt(0), 
								TreeMap::new, Collectors.toList()))
										.forEach((letter, wordList) -> {
												System.out.println("\n" + letter);
												wordList.stream().forEach(word -> System.out.println(
														String.format("%18s: %d", word.getKey().trim(), word.getValue())));
		});
	}
}
