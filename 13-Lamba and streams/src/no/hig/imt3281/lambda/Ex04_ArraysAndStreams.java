package no.hig.imt3281.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

// From Java How to program, 10th edition fig 17.6

public class Ex04_ArraysAndStreams {
	
	private static void printf (String s1, Object ...args ) {
		System.out.printf(s1, args);
	}
	
	public static void main(String[] args) {
		Integer values[] = { 2, 9, 5, 0, 3, 7, 1, 4, 8, 6 };
		
		// Display original values
		printf ("Original values: %s\n", Arrays.asList(values));

		// Sort values in ascending order with streams
		printf ("Sorted values: %s\n", Arrays.stream(values)
																.sorted()
																.collect(Collectors.toList()));
		
		// Values greater than 4
		List<Integer> greaterThan4 = Arrays.stream(values)
																.filter(value -> value > 4)
																.collect(Collectors.toList());
		printf ("Values greater than 4: %s\n", greaterThan4);
		
		// Greater than 4, sorted
		printf ("Values greater than 4, sorted: %s\n", greaterThan4.stream()
																								.parallel()
																								.sorted()
																								.collect(Collectors.toList()));
	}

}
