package no.hig.imt3281.lambda;

import java.util.Arrays;
import java.util.stream.Collectors;

// From Java How to program, 10th edition fig 17.7

public class Ex05_ArraysAndStreams2 {

	private static void printf (String s1, Object ...args ) {
		System.out.printf(s1, args);
	}

	public static void main(String[] args) {
		String strings[] = { "Red", "orange", "Yellow", "green", "Blue", "indigo", "Violet" };
		
		// Display original strings
		printf ("Original strings: %s\n", Arrays.asList(strings));
		
		// Strings in uppercase
		printf ("Strings in uppercase: %s\n", Arrays.stream(strings)
																		.map(String::toUpperCase)
																		.collect(Collectors.toList()));
		
		// Strings less than "n" (case insensitive) sorted ascending
		printf ("Strings greater than \"n\" sorted ascending: %s\n", 
				Arrays.stream(strings)
							.filter(s -> s.compareToIgnoreCase("n") <0)
							.sorted(String.CASE_INSENSITIVE_ORDER)
							.collect(Collectors.toList()));
		
		// Strings less than "n" (case insensitive) sorted descending
				printf ("Strings greater than \"n\" sorted descending: %s\n", 
						Arrays.stream(strings)
									.filter(s -> s.compareToIgnoreCase("n") <0)
									.sorted(String.CASE_INSENSITIVE_ORDER.reversed())
									.collect(Collectors.toList()));
	}
}
