package no.hig.imt3281.lambda;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Ex01ButtonMagic extends JFrame {
	
	public Ex01ButtonMagic () {
		super ("Magic button");
		JButton b = new JButton ("Press me!");
		b.addActionListener((e) -> {
			System.out.println(e);
			b.setText("That tickles");
		});
		add (b);
		JButton exit = new JButton ("Exit");
		exit.addActionListener((e)-> System.exit(0));
		add (exit, BorderLayout.SOUTH);
		pack ();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new Ex01ButtonMagic();
	}

}
