package no.hig.imt3281.lambda;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import no.hig.imt3281.lambda.employee.Employee;

// From Java How to program, 10th edition fig 17.10
public class Ex06_ProcessingEmployees {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Employee employees[] = {
				new Employee("Jason", "Red", 5000, "IT"),
				new Employee("Ashley", "Green", 7600, "IT"),
				new Employee("Matthew", "Indigo", 3587.5, "Sales"),
				new Employee("James", "Indigo", 4700.77, "Marketing"),
				new Employee("Luke", "Indigo", 6200, "IT"),
				new Employee("Jason", "Blue", 3200, "Sales"),
				new Employee("Wendy", "Brown", 4236.4, "Marketing")
		};
		
		// Get list of all employees
		List<Employee> list = Arrays.asList(employees);

		// Display all employees
		System.out.println("Complete Employee list:");
		list.stream().forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();
		
		// Represent a predicate (boolean-valued function) of one argument.
		Predicate<Employee> fourToSixThousand = e -> (e.getSalary() >= 4000 && e.getSalary() <= 6000);
		
		// Display Employees with salaries in the range $4000-$6000
		// sorted in ascending order by salary
		System.out.println("Employees earning $4000-$6000 sorted by salary:");
		list.stream().filter(fourToSixThousand)
							.sorted(Comparator.comparing(Employee::getSalary))
							.forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();
		
		// Display first Employee with salary in the range $4000-$6000
		System.out.println("First employee who earns $4000-$6000");
		System.out.println(list.stream()
											.filter(fourToSixThousand)
											.findFirst()
											.get());
		System.out.println("Press enter");
		in.nextLine();

		// Functions for getting first and last name from an Employee
		Function<Employee, String> byFirstName = Employee::getFirstName;
		Function<Employee, String> byLastName = Employee::getLastName;
		
		// Comparator for comparing Employees by last name, then by first name
		Comparator<Employee> lastThenFirst = Comparator.comparing(byLastName).thenComparing(byFirstName);
		
		// Sort employees by last name, then first name
		System.out.println("Employees in ascending order by last name, then first name");
		list.stream()
					.sorted(lastThenFirst)
					.forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();

		// Sort employees in descending order on last name, then on first name
		System.out.println("Employees in descending order by last name, then first name");
		list.stream()
					.sorted(lastThenFirst.reversed())
					.forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();

		// Display unique employee last names sorted
		System.out.println("Unique employee last name: ");
		list.stream()
					.map(Employee::getLastName)
					.distinct()
					.sorted()
					.forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();
		
		// Display only first and last names
		System.out.println("Employee names in order by last name then first name");
		list.stream()
					.sorted(lastThenFirst)
					.map(Employee::getName)
					.forEach(System.out::println);
		System.out.println("Press enter");
		in.nextLine();

		// Group Employees by department
		System.out.println("Employees by department: ");
		Map<String, List<Employee>> groupedByDepartment = list.stream()
										.collect(Collectors.groupingBy(Employee::getDepartment));
		groupedByDepartment.forEach((department, employeesInDepartment) -> {
			System.out.println(department);
			employeesInDepartment.forEach(employee -> System.out.println("\t"+employee));
		});
		System.out.println("Press enter");
		in.nextLine();

		// Count number of employees in each department
		System.out.println("Count of employees by department");
		Map<String, Long> employeeCountByDepartment = list.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
		employeeCountByDepartment.forEach((department, count) -> System.out.println (department+" has "+count+" employee(s)"));
		System.out.println("Press enter");
		in.nextLine();
		
		// Sum of Employee salaries with DoubleStream sum method
		System.out.println ("Sum of Employees' salaries (via sum method): ");
		System.out.println(list.stream().mapToDouble(Employee::getSalary).sum());
		
		// Sum of Employee salaries with stream reduce method
		System.out.println ("Sum of Employees' salaries (via stream reduce method): ");
		System.out.println(list.stream()
					.mapToDouble(Employee::getSalary)
					.reduce(0, (value1, value2) -> value1+value2));

		// Average of Employee salaries with DoubleStream average method
		System.out.println ("Average of Employees' salaries (via average method): ");
		System.out.println(String.format("%.2f", list.stream().mapToDouble(Employee::getSalary).average().getAsDouble()));

	}
}
