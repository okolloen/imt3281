package no.hig.okolloen.questions;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

public class Tutor extends JFrame {
    private JTextArea answer[];
    private JTextArea descr;
    private JLabel keyword, hostname;
    private JProgressBar pb[];
    private Vector<InetAddress> alreadyVoted = new Vector<InetAddress> ();

    public Tutor () {
        super ("Simple Quiz maker");
        JTabbedPane tabbedPane = new JTabbedPane ();
        add (tabbedPane);
        JPanel admin = new JPanel ();
        setupAdminPanel (admin);
        pb = new JProgressBar[5];
        JPanel results = new JPanel ();
        setupResultPanel (results);
        tabbedPane.addTab ("Create question", null, admin, "Create questions");
        tabbedPane.addTab ("Show results", null, results, "Show results for question");
        pack ();
        setVisible (true);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        startServer ();
    }
    
    private void startServer() {
        try {
            hostname.setText(InetAddress.getByName("128.39.243.178").getHostName());
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Thread t = new Thread () {
            public void run () {
                try {
                    ServerSocket ss = new ServerSocket (8765);
                    Socket s;
                    while ((s = ss.accept()) != null) {
                        if (alreadyVoted.contains(s.getInetAddress()))
                            continue;
                        alreadyVoted.add (s.getInetAddress());
                        BufferedReader br = new BufferedReader (new InputStreamReader (s.getInputStream ()));
                        String tmp = br.readLine();
                        if (tmp.equals(keyword.getText())) {
                            tmp = br.readLine();
                            int i = Integer.parseInt (tmp); 
                            pb[i].setValue (pb[i].getValue()+1);
                            if (pb[i].getValue()==pb[i].getMaximum()) {
                                int newMax = pb[i].getMaximum()+10;
                                for (int ii=0; ii<pb.length; ii++)
                                    pb[i].setMaximum (newMax);
                            }
                        }
                    }
                    ss.close ();                    
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        };
        t.start ();
    }

    private void setupResultPanel (JPanel p) {
        p.setLayout (new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints ();
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        for (int i=0; i<pb.length; i++) {
            p.add (new JLabel ("Answer "+i), gbc);
            gbc.gridy++;
            p.add (pb[i] = new JProgressBar(), gbc);
            gbc.gridy++;
        }
    }
    
    private void setupAdminPanel (JPanel p) {
        p.setLayout (new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints ();
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        p.add (new JLabel ("Description/explain the \"problem\""), gbc);
        gbc.gridy = 2;
        p.add (new JScrollPane (descr = new JTextArea (10, 60)), gbc);
        gbc.gridy = 3;
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.LINE_END;
        p.add (new JLabel ("Keyword: "), gbc);
        gbc.gridx = 2;
        gbc.anchor = GridBagConstraints.LINE_START;
        p.add (keyword = new JLabel ("(Used to select questionaire)"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 4;
        p.add (new JLabel ("Host: "), gbc);
        gbc.gridx = 2;
        gbc.anchor = GridBagConstraints.LINE_START;
        p.add (hostname = new JLabel ("(Used to connect to server)"), gbc);
        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.gridwidth = 2;
        answer = new JTextArea[5];
        for (int i=0; i<answer.length; i++) {
            p.add (new JLabel ("Answer "+(i+1)), gbc);
            gbc.gridy++;
            p.add (new JScrollPane (answer[i] = new JTextArea (5, 60)), gbc);
            gbc.gridy++;
        }
        gbc.gridx = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        JButton b = new JButton ("Start questionaire");
        p.add (b, gbc);
        b.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {
                newQuestionare ();
            }
        });
    }
    
    protected void newQuestionare() {
        keyword.setText(FindWord.findWord());
        for (int i=0; i<pb.length; i++) {
            pb[i].setValue (0);
            pb[i].setMaximum (10);
        }
        alreadyVoted.clear ();
    }

    public static void main (String args[]) {
        new Tutor ();
    }
}
