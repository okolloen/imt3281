package no.hig.okolloen.questions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class FindWord {
    @SuppressWarnings("resource")
    public static String findWord () {
        FileInputStream fis=null;
        try {
            File f = new File ("wordlist/wordsEn.txt");
            fis = new FileInputStream (f);
            fis.skip ((long)(Math.random()*(f.length()-500)));
            BufferedReader br = new BufferedReader (new InputStreamReader (fis));
            String tmp;
            br.readLine();
            while ((tmp = br.readLine())!=null) {
                if (tmp.length() > 4)
                    return tmp;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace ();
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return null;
    }
    
    public static void main (String args[]) {
        String s = FindWord.findWord();
        System.out.println (s);
    }
}
