package no.hig.okolloen.questions;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Student extends JFrame {
    JTextField server, keyword;
    public Student () {
        super ("Student vote");
        setLayout (new GridBagLayout ());
        GridBagConstraints gbc = new GridBagConstraints ();
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridx = 1;
        gbc.gridy = 1;
        add (new JLabel ("Host name of server: "), gbc);
        gbc.gridx = 2;
        gbc.anchor = GridBagConstraints.LINE_START;
        add (server = new JTextField (30), gbc);
        gbc.anchor = GridBagConstraints.LINE_END;
        gbc.gridx = 1;
        gbc.gridy = 2;
        add (new JLabel ("Keyword: "), gbc);
        gbc.gridx = 2;
        gbc.anchor = GridBagConstraints.LINE_START;
        add (keyword = new JTextField (30), gbc);
        JPanel p = new JPanel ();
        p.setLayout (new GridLayout (1,5));
        for (int i=0; i<5; i++) {
            JButton b = new JButton ("Answer "+(i+1));
            b.setActionCommand (i+"");
            p.add (b);
            b.addActionListener (new ActionListener () {
                public void actionPerformed (ActionEvent ae) {
                    String cmd = ae.getActionCommand ();
                    try {
                        Socket s = new Socket (server.getText(), 8765);
                        BufferedWriter bw = new BufferedWriter (new OutputStreamWriter (s.getOutputStream()));
                        bw.write (keyword.getText());
                        bw.newLine ();
                        bw.write (cmd);
                        bw.newLine ();
                        bw.flush ();
                        bw.close ();
                        s.close ();
                    } catch (UnknownHostException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    
                }
            });
        }
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridwidth = 2;
        add (p, gbc);
        pack ();
        setVisible (true);
        setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main (String args[]) {
        new Student ();
    }
}
