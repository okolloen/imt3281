package no.hig.imt3281.books;

import java.text.NumberFormat;

/**
 * The class book is used to store information about a book.
 * Contains information about title, author and price.
 * 
 * @version 1
 * @author oivindk
 *
 */
public class Book {
	// 
	private String title;
	private String author;
	private float price;
	
	/**
	 * Default constructor for the class.
	 * Initializes all members to blank values.
	 */
	public Book () {
		title = "";
		author = "";
		price = 0;
	}

	/**
	 * Constructor that takes variable for all members.
	 * 
	 * @param title a string that holds the title for the book
	 * @param author
	 * @param price
	 */
	public Book(String title, String author, float price) {
		super();
		this.title = title;
		this.author = author;
		this.price = price;
	}

	/**
	 * Returns the title of the book
	 * @return a string with the title of the book
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public String toString() {
		NumberFormat currencyFormatter;
		currencyFormatter = NumberFormat.getCurrencyInstance(Library.currentLocale);

		return getTitle()+" "+Library.messages.getString("writtenBy")+" "+
				getAuthor()+" "+Library.messages.getString("cost")+" "+currencyFormatter.format(getPrice());
	}
}
