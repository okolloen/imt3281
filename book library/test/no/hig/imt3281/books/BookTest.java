package no.hig.imt3281.books;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void testBook() {
		Book book = new Book();
		assertEquals("", book.getTitle());
		assertTrue ("The author should be a string", book.getAuthor() instanceof String);
		assertEquals(0, book.getPrice(), 0);
	}

}
