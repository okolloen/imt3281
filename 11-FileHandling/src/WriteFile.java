import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class WriteFile {
	public static void main(String[] args) {
		try {
			FileInputStream fis = new FileInputStream ("postnummerregister_ansi.txt");
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			
			FileOutputStream fos = new FileOutputStream ("test.txt");
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osw);
			String tmp = null;
			while ((tmp = br.readLine())!=null) {
				bw.write (tmp);
				bw.newLine();
			}
			br.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}		
	}
}
