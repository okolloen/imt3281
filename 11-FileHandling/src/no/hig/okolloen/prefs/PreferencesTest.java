package no.hig.okolloen.prefs;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.prefs.Preferences;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class PreferencesTest extends JFrame {
	Preferences prefs;
	
	public PreferencesTest () {
		super ("Test av preferences");
		// Henter en hashmap med preferanser for en bruker
		// Sender med getClass som parameter da dette gjør at jeg får en 
		// hashmap som er unik for dette programmet
		// Alt som lagres i denne lagres i systemets innstillinger for gjeldende bruker
		// En trenger ikke å eksplisitt si "lagre", alt som legges inn lagres fortløpende
		prefs = Preferences.userNodeForPackage( getClass() );
		// Alternativt : Preferences.systemNodeForPackage( getClass() );
		
		// getInt, get, getLong etc, første parameter er navnet på variabelen, den andre 
		// parameteren er standardverdi som returneres dersom ingen verdi er satt.
		setSize (prefs.getInt("window width", 400), prefs.getInt("window height", 400));
		setLocation (prefs.getInt("window x pos", 100), prefs.getInt("window y pos", 100));

		// Programmet bør ha fornuftige standardverdier slik at det fungerer normalt uten 
		// preferanser satt.
		
		addWindowListener (new Closing());
		setVisible (true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	class Closing extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			Dimension d = getSize();
			Point p = getLocation();
			// Rett før vinduet lukkes så lagrer vi gjeldende størrelse og plassering
			// Dermed vil vinduet dukke opp med samme størrelse og plassering neste gang programmet startes
			prefs.putInt("window width", d.width);
			prefs.putInt("window height", d.height);
			prefs.putInt("window x pos", p.x);
			prefs.putInt("window y pos", p.y);
		}
	}

	public static void main(String[] args) {
		new PreferencesTest ();
	}
}







