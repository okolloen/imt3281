package no.hig.okolloen.animal;

public class Horse extends BaseAnimal {
	private static final long serialVersionUID = 1L;
	String rider;
	
	public Horse (BaseAnimal animal) {
		super (animal);
	}

	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}
	
	public String toString () {
		return String.format("%s%s %s%s.", "Hesten heter ", super.toString(), "Hesten blir ridd av ", getRider());
	}
}
