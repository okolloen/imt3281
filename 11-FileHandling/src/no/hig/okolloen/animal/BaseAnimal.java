package no.hig.okolloen.animal;

import java.io.Serializable;

// Alle dyr har det til felles at de har et navn og en alder
public class BaseAnimal implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private int age;
	
	public BaseAnimal () {
		name = "";
		age = 0;
	}

	// Vi kan opprette et nytt dyr og ta vare på egenskapene som alle dyr har til felles
	public BaseAnimal (BaseAnimal animal) {
		name = animal.name;
		age = animal.age;
	}
	
	public BaseAnimal (String name, int age) {
		this.name = name;
		this.age = age;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString () {
		StringBuilder sb = new StringBuilder ();
		sb.append (getName());
		sb.append (" og er ");
		sb.append (getAge());
		sb.append (" år gammel.");
		return sb.toString();
	}
}