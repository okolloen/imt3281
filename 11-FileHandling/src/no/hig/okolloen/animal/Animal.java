package no.hig.okolloen.animal;

public class Animal {
	private String type;
	private String name;
	private int age;
	
	public Animal () {
		type = "";
		name = "";
		age = 0;
	}
	
	public Animal (String type, String name, int age) {
		this.type = type;
		this.name = name;
		this.age = age;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public String toString () {
		StringBuilder sb = new StringBuilder ();
		sb.append (getType());
		sb.append ("en ");
		sb.append (getName());
		sb.append (" er ");
		sb.append (getAge());
		sb.append (" år gammel.");
		return sb.toString();
	}
}