package no.hig.okolloen.model;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import no.hig.okolloen.animal.BaseAnimal;
import no.hig.okolloen.animal.Dog;
import no.hig.okolloen.animal.GoldFish;
import no.hig.okolloen.animal.Horse;

@SuppressWarnings("serial")
public class MyModel extends AbstractTableModel {
	Vector<BaseAnimal> data = new Vector<BaseAnimal>();
	String titles[] = {"Type", "Navn", "Alder"};
	JFrame mainFrame;
	
	/*
	 * Sets the frame that contains the table for this table model
	 */
	public void setTableFrame (JFrame f) {
		mainFrame = f;
	}

	@Override
	public String getColumnName(int column) {
		return titles[column];
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0 : return ("").getClass();
			case 1 : return ("").getClass();
			case 2 : return (new Integer(0)).getClass();
		}
		return ("").getClass();
	}
	
	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		BaseAnimal animal = data.get(row);
		if (col>0) {
			switch (col) {
				case 1 : return animal.getName();
				case 2 : return animal.getAge();
			}
		} else {
			if (animal instanceof Horse)
				return "Hest";
			else if (animal instanceof Dog)
				return "Hund";
			else if (animal instanceof GoldFish)
				return "Gullfisk";
			else if (animal instanceof BaseAnimal)
				return "Velg type dyr";
		}
		return null;
	}
	
	public void addAnimal () {
		BaseAnimal animal = new BaseAnimal ();
		data.add (animal);
		fireTableRowsInserted(data.size(), data.size());
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	}
	
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		BaseAnimal animal = data.get(rowIndex);
		if (columnIndex==1)
			animal.setName ((String)aValue);
		if (columnIndex==2)
			animal.setAge ((Integer)aValue);
		if (columnIndex==0) {
			BaseAnimal newAnimal = null;
			if (((String)aValue).equals("Hest")) {
				newAnimal = new Horse (animal);
				((Horse)newAnimal).setRider(JOptionPane.showInputDialog (mainFrame, "Hva heter hestens rytter?"));
			} else if (((String)aValue).equals("Hund")) {
				newAnimal = new Dog (animal);
				String tmp = JOptionPane.showInputDialog (mainFrame, "Liker hunden katter?");
				if (tmp!=null&&tmp.equalsIgnoreCase("ja"))
					((Dog)newAnimal).setLikesCats(true);
				else
					((Dog)newAnimal).setLikesCats(false);
			} else if (((String)aValue).equals("Gullfisk")) {
				newAnimal = new GoldFish (animal);
				((GoldFish)newAnimal).setBowlPlacement(JOptionPane.showInputDialog (mainFrame, "I hvilket rom står gullfiskbollen?"));
			}
			data.set(rowIndex, newAnimal);
		}
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	public void save (ObjectOutputStream oos) {
		try {
//			for (int i=0; i<data.size(); i++)
//				oos.writeObject(data.get(i));
			oos.writeObject(data);
		} catch (IOException ioe) {
			System.err.println ("Feil under skriving til fil");
		}
	}
	
	public void load (ObjectInputStream ois) {
		data.clear();
		try {
/*			while (true) {
				BaseAnimal animal = (BaseAnimal)ois.readObject();
				data.add(animal);
			}*/
			data = (Vector<BaseAnimal>)ois.readObject();
		} catch (EOFException eofe) {
			// Slutt på fila
		} catch (ClassCastException cce) {
			System.err.println ("OPPPSSSSS, dette var ingen dyrehage");
		} catch (ClassNotFoundException cnfe) {
			System.err.println ("Oi, vi har mista ur-dyret.");
		} catch (IOException ioe) {
			System.err.println ("Feil under lesing fra fil");
		} finally {
			fireTableDataChanged();
		}
	}
	
	public Vector<BaseAnimal> getData () {
		return data;
	}
}



