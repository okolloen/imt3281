import javax.swing.*;
import javax.swing.table.*;

public class CompleteWeatherLog extends JFrame {
  JTable weatherTable = null;
  WeatherModel wm = new WeatherModel ();

  public CompleteWeatherLog () {
    super ("Weather log");
    wm.addRow (new WeatherItem (WeatherItem.SUNSHINE, "A bright sunny day", new java.util.Date()));
    wm.addRow (new WeatherItem (WeatherItem.CLOUDY_RAIN, "Evening rainstorm", new java.util.Date()));

    weatherTable = new JTable (wm);

		// Set up drop down box as editor and use image as renderer
    JComboBox weatherTypes = new JComboBox (WeatherItem.getDescriptions());
    weatherTypes.setRenderer (new WeatherDropDownRenderer ());
    TableColumn typeColumn = weatherTable.getColumnModel().getColumn (0);
    typeColumn.setCellEditor (new DefaultCellEditor(weatherTypes));
    typeColumn.setCellRenderer (new WeatherTypeRenderer());
		
		// Lock the with of the columns
    weatherTable.getColumnModel().getColumn(0).setPreferredWidth(70);
    weatherTable.getColumnModel().getColumn(1).setPreferredWidth(250);
    weatherTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		// And the above is useless unless we tell the table to not automatically resize
    weatherTable.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);

    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    CompleteWeatherLog bwl = new CompleteWeatherLog ();
  }
}