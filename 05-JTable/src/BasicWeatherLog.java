import javax.swing.*;

public class BasicWeatherLog extends JFrame {
	// JTable is used for tabular data display and editing
  JTable weatherTable = null;

  public BasicWeatherLog () {
    super ("Weather log");
		// A JTable has titles on each column
    String title[] = { "Weather", "Description", "Date" };
		// and the data is in its simplest form a two dimensional array
    Object weatherData[][] = { { "Sunny", "A bright sunny day", new java.util.Date() }, { "Rain", "Evening rainstorm", new java.util.Date() } };

    weatherTable = new JTable (weatherData, title);
		// Add the table inside a scroll pane
    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
  }

  public static void main (String args[]) {
    BasicWeatherLog bwl = new BasicWeatherLog ();
  }
}